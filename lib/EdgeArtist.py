import matplotlib.patches as patches
from matplotlib.lines import Line2D
import bzmagPy as bzmag

#-----------------------------------------------------------------------------
class EdgeArtist(Line2D):
    """An Edge patch."""
    def __str__(self):
        return "EdgeArtist()"
    
    # ------------------------------------------------------------------------
    # based on Data coordinates
    def __init__(self, node, edgeID, **kwargs):
        super().__init__([0], [0], **kwargs)
        self.setEdge(node, edgeID)
        self.set_lw(0.1)
        self.set_pickradius(3)
        self.set_picker(3)

    # ------------------------------------------------------------------------
    def setEdge(self, node, edgeID):
        if node.getNumberOfEdge() < edgeID: return
        
        self._node = node
        self._edgeID = edgeID
        
        path = node.getEdgePath(edgeID)
        self._x = path[0:len(path):3]
        self._y = path[1:len(path):3]
        
        self.set_data(self._x, self._y)
        
        
    # ------------------------------------------------------------------------
    def getNode(self):
        return self._node
        
    # ------------------------------------------------------------------------
    def getEdgeID(self):
        return self._edgeID
        
    # ------------------------------------------------------------------------
    def getType(self):
        return 2
            
    # ------------------------------------------------------------------------
    def disablePicker(self):
        self.set_pickradius(0)
        
    # ------------------------------------------------------------------------
    def enablePicker(self):
        self.set_pickradius(3)
        
    # ------------------------------------------------------------------------
    def setSelectedStatus(self, status):
        if status:
            self.set_lw(1)
            self.set_markeredgecolor('r')
        else:
            self.set_lw(0.1)
            self.set_markeredgecolor('b')
    