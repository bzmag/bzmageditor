import matplotlib.patches as patches
from matplotlib.path import Path
import bzmagPy as bzmag

#-----------------------------------------------------------------------------
class SurfaceArtist_new(patches.Patch):
    """An Surface patch."""
    def __str__(self):
        return "SurfaceArtist_new()"
    
    _path = Path([[0.0, 0.0]])
    _xmin = 0
    _xmax = 0
    _ymin = 0
    _ymax = 0
    _node = None
    
    # ------------------------------------------------------------------------
    # based on Data coordinates
    def __init__(self, node, **kwargs):
        super().__init__(**kwargs)
        
        self.setNode(node)
        
    # ------------------------------------------------------------------------
    def getNode(self):
        return self._node
        
    # ------------------------------------------------------------------------
    def getType(self):
        return 1
    
    # ------------------------------------------------------------------------
    def setNode(self, node):
        path = node.getPath(0.5)
        self._node = node
        
        x = path[0:len(path):3]
        y = path[1:len(path):3]
        
        #
        if len(x) ==0 or len(y) == 0:
            self._path = Path([[0.0, 0.0]])
            return
        
        x_min = min(x)
        x_max = max(x)
        y_min = min(y)
        y_max = max(y)
        self.set_xlim(x_min, x_max)
        self.set_ylim(y_min, y_max)
        
        verts = list(zip(x, y))
        #print(verts)
        
        codes = path[2:len(path):3]
        color = [x/255 for x in node.Color]
        color[3] = 1 - color[3]
        
        self._path = Path(verts, codes)
        self.set_facecolor(color)
        self.set_edgecolor('black')
        self.set_linewidth(0.1)
        self.set_picker(self.polyhole_picker)

    # ------------------------------------------------------------------------
    def set_xlim(self, min, max):
        self._xmin = min
        self._xmax = max
        
    # ------------------------------------------------------------------------
    def set_ylim(self, min, max):
        self._ymin = min
        self._ymax = max

    # ------------------------------------------------------------------------
    def get_path(self):
        return self._path
        
    # ------------------------------------------------------------------------
    def boundingRect(self):
        return (self._xmin, self._xmax, self._ymin, self._ymax)
    
    # ------------------------------------------------------------------------
    def setSelected(self, selected):
        if selected == True:
            self.set_linewidth(1)
            self.set_linestyle('dashed')
            self.set_edgecolor('blue')
        else:
            self.set_linewidth(0.1)
            self.set_linestyle('solid')
            self.set_edgecolor('black')
            
        
    # ------------------------------------------------------------------------           
    def polyhole_picker(self, path, mouseevent):
        #nodeID = self._node.getID()
        
        x = mouseevent.xdata
        y = mouseevent.ydata
        
        if self.get_visible() and self._node.hitTest(x, y) :
            return True, dict()
        
        else:
            return False, dict()
            