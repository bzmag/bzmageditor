import bzmagPy as bzmag
import numpy as np

class BCNodeHelper:
    # ------------------------------------------------------------------------
    def __init__(self, BCNodes, FENodes):
        # 경계조건 노드
        self.BC_Nodes_ = BCNodes
        
        # 유한요소 해석에 사용되는 절점
        self.FE_Nodes_ = FENodes
        
        # key == 0 : fixed BC
        # key == 1 : periodic BC
        self.FBCs_ = np.array([])
        self.PBCs_ = np.array([])
        self.dup_count_ = 0
        
        self.idxMap_  = dict()
        self.idxRMap_ = dict()
        self.sgnMap_  = dict()
        self.sgnRMap_ = dict()
        
        self.makeBCStrucure()
        self.make_index_map()
        
    # ------------------------------------------------------------------------
    def FixBC(self):
        return self.FBCs_
        
    # ------------------------------------------------------------------------
    def PeriodicBC(self):
        return self.PBCs_
        
        
    # ------------------------------------------------------------------------
    def NumberOfPeriodicBCs(self):
        if len(self.PBCs_) > 0:
            return len(self.PBCs_[0]) - self.dup_count_
        else:
            return 0
        
    # ------------------------------------------------------------------------
    def getIndexMaps(self):
        return [self.idxMap_, self.sgnMap_, self.idxRMap_, self.sgnRMap_]
        
    # ------------------------------------------------------------------------
    def makeBCStrucure(self):
        # fixed BCs
        self.FBCs_ = np.array([])
        
        # periodic BCs
        self.PBCs_ = np.array([])
        
        #print(self.BC_Nodes_, len(self.BC_Nodes_), self.BC_Nodes_)
        
        
        for BCID, nodeIDs in self.BC_Nodes_.items():
            BC = bzmag.getObject(BCID)
            type_name = BC.getTypeName()
            if type_name == 'FixedBCNode':
                bc_value = np.ones(len(nodeIDs)) * BC.getBCValue()
                fixed_bcs = np.stack((nodeIDs, bc_value))
                
                # FBCs = [[nodeID, value], [], ...]
                if len(self.FBCs_) > 0:
                    self.FBCs_ = numpy.concatenate((self.FBCs_, fixed_bcs), axis=1)
                else:
                    self.FBCs_ = fixed_bcs
                
                # 나중에 BC적용시...
                #for bc in BCs_[type_name].transpose():
                #    apply BC
                
            elif type_name == 'SlaveBCNode':
                # get a refernce node (master node)
                Ref = BC.Reference
                
                # type of the reference node should be 'PeriodicBCNode'
                if Ref.getTypeName() != 'MasterBCNode':
                    print('Something wrong in a periodic boudnary condition!!')
                    
                # get node IDs of the reference nodes and 
                # the number of the IDs should be the same as itself's
                ref_nodeIDs = self.BC_Nodes_[BC.Reference.getID()]
                
                if len(nodeIDs) != len(ref_nodeIDs):
                    print('the number of nodes on master and slave should be the same!')
                
                # node들을 origin 기준 소팅해야 함
                # master NodeIDs
                #print('before sort', nodeIDs)
                sNodes = self.sort(BC, nodeIDs)
                #print('after sort', sNodes)
                
                # slave NodeIDs
                mNodes = self.sort(Ref, ref_nodeIDs)
                
                
                # make bc data structures
                # [masterID, slaveID, even?], [], ...
                bc_values = np.ones(len(sNodes))
                if not BC.EvenRelation:
                    bc_values = -1 * bc_values;
                    
                # PBCs = [[masterID, slaveID, relation], [], ...]
                periodic_bcs = np.stack((mNodes, sNodes, bc_values))
                if len(self.PBCs_) > 0:
                    self.PBCs_ = numpy.concatenate((self.PBCs_, periodic_bcs), axis=1)
                else:
                    self.PBCs_ = periodic_bcs
                
                #print('Periodic Boundary :[[master], [slave], [relation]')
                #print('PBC: ',self.PBCs_)
                    
    # ------------------------------------------------------------------------
    def sort(self, BCNode, nodeIDs):
        #return nodeIDs
        # 경계커브의 시작점 혹은 끝점 기준 경계 커브에 존재하는 절점의 거리를 구한후
        # 이를 소팅하여 master/slave 짝을 맞춘다
        data = dict()
        for nodeID in nodeIDs:
            node = self.FE_Nodes_[nodeID]
            x, y = node['point']
            BCidx = node['marker']
            distance = BCNode.getDistanceFromtheOrigin(x, y, BCNode.Orientation)
            data[nodeID] = distance
            
        #sortedNodeIDs = sorted(data.items(), key=lambda x:x[1])
        #print('Sorted', sortedNodeIDs)
        sortedNodeIDs = sorted(data, key=data.get)
        #print('Sorted', nodeIDs, sortedNodeIDs)
        return sortedNodeIDs
    
    # ------------------------------------------------------------------------
    def make_index_map(self):
        # 주기경계 처리
        masterNodes = np.array([])
        slaveNodes = np.array([])
        relations  = np.array([])
        if len(self.PBCs_) > 0:
            masterNodes = self.PBCs_[0]
            slaveNodes  = self.PBCs_[1]
            relations   = self.PBCs_[2]
            
            # master와 slave가 같은 노드를 가르키는 경우
            # 주기경계 노드에서 제외
            arridx = np.where(masterNodes == slaveNodes)
            if len(arridx[0]) >1 : 
                print('there are two or nodes common to master and slave!!')
                return 
            
            if len(arridx[0]) == 1:
                # 주기경계(마스트/슬레이브간) 중첩노드
                specialNode = slaveNodes[arridx][0]
                
                # 고정경계에 추가
                additem = np.array([[specialNode, 0]])
                if len(self.FBCs_) > 0:
                    self.FBCs_  = np.append(self.FBCs_.transpose(), additem, axis=0).transpose()
                else:
                    self.FBCs_ = additem.transpose()
                
                # 기존 주기경계에서 중첩노드 삭제
                masterNodes = np.delete(masterNodes, arridx)
                slaveNodes  = np.delete(slaveNodes,  arridx)
                relations   = np.delete(relations,   arridx)
                
                # 주기경계조건 업데이트
                self.PBCs_  = np.stack((masterNodes, slaveNodes, relations))
        
        #고정경계 처리
        PBCs = self.PBCs_
        fixedNodes = np.array([])
        if len( self.FBCs_) > 0:
            fixedNodes  = self.FBCs_[0]
        
            # 주기경계와 고정경계 중첩노드 검색/삭제
            #dupNodes = masterNodes[np.where(np.isin(masterNodes, fixedNodes))]
            #print('duplicated nodes: ', dupNodes)
            arridx = np.where(np.isin(masterNodes, fixedNodes))
            if len(arridx[0]) > 0:
                masterNodes = np.delete(masterNodes, arridx)
                slaveNodes  = np.delete(slaveNodes,  arridx)
                relations   = np.delete(relations,   arridx)
                self.dup_count_ = 1
                
                # slave 노드에 대해서도 검색하면 동일한 인덱스를 얻는다
                #dupNodes = slaveNodes[np.where(np.isin(slaveNodes, fixedNodes))]
                #arridx = np.where(np.isin(masterNodes, fixedNodes))
                #slaveNodes  = np.delete(slaveNodes,  arridx)
                #print('duplicated nodes: ', dupNodes)
                #print('index of dup nodes:', arridx1, arridx2)
                
                # 주기경계 (경계 중복 제거)
                PBCs = np.stack((masterNodes, slaveNodes, relations))
                
        
        self.idxMap_  = dict()
        self.idxRMap_ = dict()
        self.sgnMap_  = dict()
        self.sgnRMap_ = dict()
        bcNodes = np.append(fixedNodes, slaveNodes, axis=0)
        bcNodes = np.unique(bcNodes)
        
        # re-numbering nodeIDs...
        new_idx = 0
        nSize = len(self.FE_Nodes_)
        
        # 일반노드들을 우선 배치함
        for idx in np.arange(nSize):
            if not idx in bcNodes:
                idx = int(idx)
                self.idxMap_[idx] = new_idx
                self.sgnMap_[idx] = 1
                
                self.idxRMap_[new_idx] = idx
                self.sgnRMap_[new_idx] = 1
                
                new_idx = new_idx + 1
                
        #print('final_idx:', new_idx-1)
        # 고정경계노드를 배치함
        for idx in fixedNodes:
            idx = int(idx)
            self.idxMap_[idx] = new_idx
            self.sgnMap_[idx] = 1
            
            self.idxRMap_[new_idx] = idx
            self.sgnRMap_[new_idx] = 1
            
            new_idx = new_idx + 1
        
        
        # 주기경계(slave노드)를 마지막 배치함
        for pbc in PBCs.transpose():
            slave    = int(pbc[1])
            relation = pbc[2]   # 1 or -1
            
            self.idxMap_[slave] = new_idx
            self.sgnMap_[slave] = relation
            
            self.idxRMap_[new_idx] = slave
            self.sgnRMap_[new_idx] = relation
            
            new_idx = new_idx + 1
        
        
        #print('index map:', self.idxMap_)
        # Slave 노드가 Master노드를 참조 할 수 있게 함
        for pbc in PBCs.transpose():
            master   = int(pbc[0])
            slave    = int(pbc[1])
            relation = pbc[2]
            tidx = self.idxMap_[slave]
            self.idxMap_[slave] = self.idxMap_[master]
            self.idxRMap_[tidx] = master
            
    
        print('complete to make index map!')
        #print(self.idxMap_)
    