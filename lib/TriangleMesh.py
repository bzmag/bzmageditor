# -*- coding: utf-8 -*-
import sys, os, io
import math
import triangle
import numpy as np
import bzmagPy as bzmag
import math
import csv

#-----------------------------------------------------------------------------
class TriangleMesh:
    def __init__(self):
        super(TriangleMesh, self).__init__()
        
        # triangle input data (*.poly)
        self.input_ = {}
        
        # trinagle output data (mesh results)
        self.output_ = {}
        
        #
        #self.g_vertices = np.empty(shape=[0, 2])
        #self.g_segments = np.empty(shape=[0, 2])
        #self.g_regions  = np.empty(shape=[0, 4])
        
        
    # ------------------------------------------------------------------------
    def generateMesh(self, root_path, bc_path):
        tri_node = bzmag.get('/sys/server/triangle')
        
        # 경계조건 링크를 우선해야 한다
        tri_node.linkBC(bc_path)
        if not tri_node.makePolyStructure(root_path):
            print("Fail to generate mesh")
            return False
        # Step1. Polyhole 의 갯수만큼 루프를 돌며 요소 생성을 위한 기저 절점과 세그먼트 생성
        #        (.Poly 파일 구조 생성이라고 보면 편할듯)
        #        g_verices (절점), g_segments(세그먼트), g_holes(홀), g_regions(영역)에 저장 될 것임
        # Step2. 현재 Polyhole을 지정할 수 있는 임의 좌표 <x> <y>를 만들고
        #        이를 활용하여 해당 영역에 <attribute> 과 <maximum area>를 설정
        # Step3. Mesh Generating
        

        # 현재 영역을 구성하는 절점좌표를 받음
        # 튜플형태(x0, y0, x1, y1, ...)로 넘어오게 되는데,
        # 이를 reshape 해서 2차원 배열로 만들어 편리하게 사용할 것임
        v_data = tri_node.getVertices()
        nvertices = len(v_data) / 2
        vertices = np.array(v_data).reshape(int(nvertices), 2)
        
        # 마찬가지 segments 데이터도 튜플형태로 넘어오는데
        # (시작점0_ID, 끝점0_ID, 시작점1_ID, 끝점1_ID, ...) 형태로 넘어옴
        # 마찬가지로 reshape 해서 편리하게 사용함
        s_data = tri_node.getSegments()
        nsegments = len(s_data) / 2
        segments = np.array(s_data).reshape(int(nsegments), 2)
        
        # 세그먼트 마커도 불러온다
        segment_markers = tri_node.getSegmentMarkers()
        #print(segment_markers)
        
        # 영역은 좌표(x,y), <attribute> 그리고 요소컨트롤을 위한 <maximum area>를 설정할 수 있음
        # <attribute>는 영역의 ID를 설정하기로 하며, 
        # <maximum area>는 사용자로부터 받은 값을 설정하면 좋은데 아직 구현이 안되었음
        # getRegions() 매써드는 [x,y,ID], [x,y,ID] ,,, 형태로 데이터를 넘겨줌
        r_data = np.array(tri_node.getRegions())
        #print(r_data)
        nregions = len(r_data) / 4
        regions = np.array(r_data).reshape(int(nregions), 4)
        #regions = np.array(r_data).reshape(-1, 4)
        
        # 홀데이터
        h_data = tri_node.getHoles()
        nholes = len(h_data) / 2
        holes = np.array(h_data).reshape(int(nholes), 2)
            
        self.input_ = dict(vertices=vertices, segments=segments, segment_markers=segment_markers, regions=regions, holes=holes)
        self.writePoly(self.input_)
        #홀이 없으면 딕셔너리에서 지운다; 그렇지 않으면 요소생성시 오류발생
        if len(holes)==0: del self.input_['holes']
        #print(input)
        
        #self.output_ = triangle.triangulate(self.input_, 'pqAaY')
        #self.output_ = triangle.triangulate(self.input_, 'pqYY')
        self.output_ = triangle.triangulate(self.input_, 'pqAaY')
        #print(self.output_)
        
        # Write .poly file
        '''
        vertices = self.output_['vertices']
        vertex_markers = self.output_['vertex_markers']
        segments = self.output_['segments']
        segment_markers = self.output_['segment_markers']
        regions = self.output_['regions']
        triangles = self.output_['triangles']
        triangle_attribute = self.output_['triangle_attribute']
        
        f = open('input.mesh', 'w', encoding='utf-8', newline='')
        wr = csv.writer(f)
        #vertices
        wr.writerow('# vertices')
        wr.writerow([len(vertices), 2, 0, 1])
        for i, v in enumerate(vertices):
            wr.writerow([i, v[0], v[1], vertex_markers[i]])
        print("write file...\n")
        #segments
        wr.writerow('# segments')
        wr.writerow([len(segments), 1])
        for i, s in enumerate(segments):
            wr.writerow([i, s[0], s[1], segment_markers[i]])
        
        #elements
        wr.writerow('# elements')
        wr.writerow([len(triangles), 3, 1])
        for i, t in enumerate(triangles):
            wr.writerow([i, t[0], t[1], t[2], triangle_attribute[i]])
        
        #regional attributes and/or area constraints
        f.close()
        '''
        return True
    
    def writePoly(self, input):
        f = open('input.mesh', 'w', encoding='utf-8', newline='')
        
        vertices = input['vertices']
        data = "%i 2 0 1\n" % len(vertices)
        f.write(data)
        for i, v in enumerate(vertices):
            data = "%i %f %f\n" % (i+1, v[0], v[1])
            f.write(data)
            
        segments = input['segments']
        segment_markers = input['segment_markers']
        data = "%i 1\n" % len(segments)
        f.write(data)
        for i, s in enumerate(segments):
            data = "%i %i %i %i\n" % (i+1, s[0]+1, s[1]+1, segment_markers[i])
            f.write(data)
            
        holes = input['holes']
        data = "%i\n" % len(holes)
        f.write(data)
        for i, h in enumerate(holes):
            data = "%i %f %f\n" % (i+1, h[0], h[1])
            f.write(data)
        
        regions = input['regions']
        data = "%i\n" % len(regions)
        f.write(data)
        for i, r in enumerate(regions):
            data = "%i %f %f %i %i\n" % (i+1, r[0], r[1], r[2], r[3])
            f.write(data)
        
        f.close()
    # ------------------------------------------------------------------------
    def getMeshData(self):
        return self.output_