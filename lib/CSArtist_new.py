from matplotlib.transforms import (Affine2D, IdentityTransform, ScaledTranslation)
import matplotlib.patches as patches
import matplotlib.artist as artist
from matplotlib.path import Path
import matplotlib.cbook as cbook
import matplotlib.transforms as transforms
import bzmagPy as bzmag
import numpy as np

#-----------------------------------------------------------------------------
class CSArtist_new(patches.Patch):
    """An CS patch."""
    def __str__(self):
        return "CS_Arrow()"

    # x축 arrow
    _path = Path([[0.0, 0.1], [0.0, -0.1],
                  [0.8, -0.1], [0.8, -0.3],
                  [1.0, 0.0], [0.8, 0.3],
                  [0.8, 0.1], [0.0, 0.1]],
                 closed=True)
                  

    # ------------------------------------------------------------------------
    def __init__(self, axes, node=None, dx=1.0, dy=1.0, width=1.0, **kwargs):
        """
        Draws an arrow from (*x*, *y*) to (*x* + *dx*, *y* + *dy*).
        The width of the arrow is scaled by *width*.

        Parameters
        ----------
        dx : float
            Arrow length in the x direction.
        dy : float
            Arrow length in the y direction.
        width : float, default: 1
            Scale factor for the width of the arrow. With a default value of 1,
            the tail width is 0.2 and head width is 0.6.
        **kwargs
            Keyword arguments control the `Patch` properties:

            %(Patch)s

        See Also
        --------
        FancyArrow
            Patch that allows independent control of the head and tail
            properties.
        """
        super().__init__(**kwargs)
        self.set_figure(axes.figure)
        self.axes = axes
        self.dx = dx
        self.dy = dy
        self.width = width
        self.setNode(node)

    # ------------------------------------------------------------------------
    def setNode(self, node):
        if node != None:
            [m11, m12, m13, m21, m22, m23] = node.getTransformation()
            #print(m13, m23)
            #https://matplotlib.org/3.1.0/tutorials/advanced/transforms_tutorial.html
            mtx = np.array([[m11, m12, 0],
                            [m21, m22, 0],
                            [0,   0,   1]])
            axes = self.axes
            trans = (axes.figure.dpi_scale_trans + ScaledTranslation(m13, m23, axes.transData))
            self.set_transform(trans)
                            
            refcs = Affine2D(matrix=mtx)
            dx_y, dy_y = refcs.transform((0.0, self.dy))
            dx_x, dy_x = refcs.transform((self.dx, 0.0))
            
            self._patch_transform_x = (
                transforms.Affine2D()
                .scale(np.hypot(dx_x, dy_x), self.width)
                .rotate(np.arctan2(dy_x, dx_x))
                .translate(0, 0)
                .frozen())
                
            self._patch_transform_y = (
                transforms.Affine2D()
                .scale(np.hypot(dx_y, dy_y), self.width)
                .rotate(np.arctan2(dy_y, dx_y))
                .translate(0, 0)
                .frozen())
        
    # ------------------------------------------------------------------------
    def get_path(self):
        return self._path
        
    # ------------------------------------------------------------------------
    def get_transform_x(self):
        """Return the `~.transforms.Transform` applied to the `Patch`."""
        return self.get_patch_transform_x() + artist.Artist.get_transform(self)
        
    # ------------------------------------------------------------------------
    def get_transform_y(self):
        """Return the `~.transforms.Transform` applied to the `Patch`."""
        return self.get_patch_transform_y() + artist.Artist.get_transform(self)
        
        
    # ------------------------------------------------------------------------
    def get_patch_transform_x(self):
        return self._patch_transform_x
        
    # ------------------------------------------------------------------------
    def get_patch_transform_y(self):
        return self._patch_transform_y
        
    # ------------------------------------------------------------------------
    def draw(self, renderer):
        # docstring inherited
        if not self.get_visible():
            return
            
        # Patch has traditionally ignored the dashoffset.
        path = self.get_path()
        
        transform = self.get_transform_x()
        tpath = transform.transform_path_non_affine(path)
        affine = transform.get_affine()
        self.set_facecolor('b')
        self._draw_paths_with_artist_properties(
            renderer,
            [(tpath, affine,
              # Work around a bug in the PDF and SVG renderers, which
              # do not draw the hatches if the facecolor is fully
              # transparent, but do if it is None.
              self._facecolor if self._facecolor[3] else None)])
              
        transform = self.get_transform_y()
        tpath = transform.transform_path_non_affine(path)
        affine = transform.get_affine()
        self.set_facecolor('r')
        self._draw_paths_with_artist_properties(
            renderer,
            [(tpath, affine,
              # Work around a bug in the PDF and SVG renderers, which
              # do not draw the hatches if the facecolor is fully
              # transparent, but do if it is None.
              self._facecolor if self._facecolor[3] else None)])