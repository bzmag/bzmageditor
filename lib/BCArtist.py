import matplotlib.patches as patches
from matplotlib.lines import Line2D
import bzmagPy as bzmag

#-----------------------------------------------------------------------------
class BCArtist(Line2D):
    """An Edge patch."""
    def __str__(self):
        return "BCArtist()"
    
    # ------------------------------------------------------------------------
    # based on Data coordinates
    def __init__(self, node, **kwargs):
        super().__init__([0], [0], **kwargs)
        self.setEdge(node)
        
        self.set_pickradius(3)

    # ------------------------------------------------------------------------
    def setEdge(self, node):

        self._node = node
        
        path = node.getPath()
        self._x = path[0:len(path):3]
        self._y = path[1:len(path):3]
        
        self.set_data(self._x, self._y)
        self.set_lw(0.5)
        
    # ------------------------------------------------------------------------
    def getNode(self):
        return self._node
        
    # ------------------------------------------------------------------------
    def getEdgeID(self):
        return self._edgeID
        
    # ------------------------------------------------------------------------
    def getType(self):
        return 2
            
    # ------------------------------------------------------------------------
    def disablePicker(self):
        self.set_pickradius(0)
        
    # ------------------------------------------------------------------------
    def enablePicker(self):
        self.set_pickradius(3)
        
    # ------------------------------------------------------------------------
    def setSelectedStatus(self, status):
        if status:
            self.set_linewidth(1)
            self.set_markeredgecolor('red')
        else:
            self.set_linewidth(0.1)
            self.set_markeredgecolor('blue')
    
