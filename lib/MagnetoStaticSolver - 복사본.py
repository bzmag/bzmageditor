# -*- coding: utf-8 -*-
import sys, os, io
from pathlib import Path
import math
import triangle
import numpy as np
from scipy.interpolate import CubicSpline
from scipy.interpolate import interp1d
from scipy.sparse import csr_matrix
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import LinearOperator
from scipy.sparse.linalg import spsolve
import bzmagPy as bzmag
from matplotlib.transforms import (Affine2D)

def fp(inter_bhcurve, x):
    eps = 1e-4
    return (inter_bhcurve(x+eps) - inter_bhcurve(x))/eps
    
#-----------------------------------------------------------------------------
class MagnetoStaticSolver:
    def __init__(self):
        super(MagnetoStaticSolver, self).__init__()
        self.FE_Elements_ = {}
        self.FE_Nodes_    = {}
        self.FE_Source_   = {}
        self.FE_Regions_  = {}
        self.FE_Materials_= {}
        self.FE_Sources_  = {}
        
        self.FE_BCs_      = {}
        self.BC_Nodes_    = {}
        
        # Global Matrix and forcing vector
        #self.K_          = np.array([])
        #self.J_          = np.array([])
        
        # Solution (vector potentials)
        #self.A_          = np.array([])
        
        # Solution (Flux denisty)
        #self.B_          = np.array([])
    
    #-------------------------------------------------------------------------
    def setMeshData(self, data):
        tri_node = bzmag.get('/sys/server/triangle')
        
        # 'vertices': array([[x, y],...])
        verts        = data['vertices']
        
        # 'vertex_markers': array([[0],...], dtype=int32)
        verts_marker = data['vertex_markers'][:,0]
        
        # 'segments': array([[NodeID1, NodeID2], ...], dtype=int32)
        segments = data['segments']
        
        # 'segment_markers': array([[0],...], dtype=int32)
        segment_markers = data['segment_markers'][:,0]
        
        #'triangles': array([[NodeID1, NodeID2, NodeID3],...], dtype=int32)
        tris         = data['triangles']
        
        #'triangle_attribute': array([[RegionID],...])
        tris_attr    = data['triangle_attributes'][:,0]
        
        # 'regions': array([[x, y, RegionID, Area],...])
        regions      = data['regions']
        
        # make FE Nodes data
        self.FE_Nodes_.clear()
        for i, vert in enumerate(verts):
            self.FE_Nodes_[i] = {'point': vert, 'marker': int(verts_marker[i])}
        
        
        # make FE Elements data
        self.FE_Elements_.clear()
        for i, tri in enumerate(tris):
            dim = len(tri)
            self.FE_Elements_[i] = {'dim':dim, 
                                    'nodes': tri, 
                                    'J': 0 , 
                                    'mu_x' : 1, 
                                    'mu_y' : 1, 
                                    'Mx' : 0, 
                                    'My' : 0, 
                                    'regionID' : int(tris_attr[i]),
                                    'SS' : np.zeros((dim,dim)),
                                    'A' : np.zeros(dim),
                                    'Q' : np.zeros(dim)}
        
        # make FE Regions data
        self.FE_Regions_.clear()
        for region in regions:
            regionID = int(region[2])
            
            # 영역의 면적 구함--> 추후 전류밀도 구하기 위함임
            area = tri_node.getDomainArea(regionID)
            head = bzmag.getObject(regionID)
            material = head.Material
            if material == None:
                materialID = -1
            else:
                materialID = material.getID()
                
            #print('make material', materialID)
            if regionID not in self.FE_Regions_:
                # 소스 ID는 추후 재설정될 것이며 기본값으로 -1로 셋팅한다
                elementIDs = np.where(tris_attr == regionID)
                self.FE_Regions_[regionID] = {'sourceID' : -1, 'materialID' : materialID, 'area' : area, 'elements' : elementIDs[0]}
        
                #print('Element ID',elementIDs)

        # make FE Boundary data
        self.BC_Nodes_.clear()
        for i, maker in enumerate(segment_markers):
            if maker > 0:
                seg1, seg2  = segments[i]
                maker = int(maker)
                if maker in self.BC_Nodes_:
                    self.BC_Nodes_[maker] = np.append(self.BC_Nodes_[maker], [seg1, seg2])
                else:
                    self.BC_Nodes_[maker] = [seg1, seg2]
                    
        for key, value in self.BC_Nodes_.items():
            value = np.unique(value)
            self.BC_Nodes_[key] = value
            
        #print(self.BC_Nodes_)
                
    #-------------------------------------------------------------------------
    def setMaterials(self, matNodes):
        # 아이디 -1은 vaccume 재질이다 (기본 재질)
        self.FE_Materials_.clear()
        self.FE_Materials_[-1] = {'sigma': 0, \
                                  'M': 0, \
                                  'dirM': (0,0), \
                                  'mur': 1, \
                                  'BH': None, \
                                  'B2v': None}
                                  
        # 이제 재질 데이터를 만든다
        for node in matNodes.getChildren():
            if 'MaterialNode' == node.getTypeName():
                nodeID = node.getID()
                BH = None
                B2v = None
                # 이하는 테스트 코드임
                # 임의로 투자율이 1000이 넘으면 비선형 재질로 간주해 봄
                mur = float(node.Permeability)
                if mur > 1000:
                    [BH, B2v] = self.loadBHCurve('35PN210_BH.tab')
                    
                self.FE_Materials_[nodeID] = {'sigma': float(node.Conductivity), \
                                              'M': float(node.Magnetization), \
                                              'dirM': (float(node.Mvector[0]), float(node.Mvector[1])), \
                                              'mur': mur, \
                                              'BH': BH, \
                                              'B2v': B2v}
                                              
                                              
    #-------------------------------------------------------------------------
    def setSources(self, sourceNodes):
        self.FE_Sources_.clear()
        for node in sourceNodes.getChildren():
            if 'WindingNode' == node.getTypeName():
                nodeID = node.getID()
                current = node.Current
                a = node.ParallelBranches
                #print('Current/ParallelBranches', current, a)
                
                coil_info = []
                for coil in node.getChildren():
                    head = coil.getReferenceNode()
                    
                    # 이미 만들어진 FE_Region(HeadNode별로 하나씩 만들어졌음)에
                    # 소스 ID를 할당한다. 기본값은 -1로 설정되어 있음
                    #print('Winding Object ID:', head.getID())
                    self.FE_Regions_[head.getID()]['sourceID'] = nodeID
                    turns = coil.Turns
                    direction = coil.Direction
                    
                    # GeomHeadNode, 턴수, 방향성
                    coil_info.append([head, turns, direction])
                    
                self.FE_Sources_[nodeID] = {'I' : current, 'ParallelBranches' : a, 'Coil' : coil_info}
                
        #print(self.FE_Sources_)

    #-------------------------------------------------------------------------
    def loadBHCurve(self, file):
        mu0 = 4 * np.pi * 1e-7
        bh_data = list()

        file_path = Path() / os.getcwd() / file
        file_path.touch()
        f = open(file_path, 'r', encoding='utf-8')

        for aRow in f:
            # print(aRow)
            line = aRow.split('\t')
            HB = np.array([line[0], line[1]])
            HB = HB.astype(np.float)
            # print(HB)
            bh_data.append(HB)
        f.close()
        # print(HB)

        # CubicSpline 보간을 위해 기울기가 mu0 인 데이터 점을 3개 더 추가한다.
        H_max = HB[0]
        B_max = HB[1]
        for i in range(10):
            H_ex = H_max * (2 + i * 10)
            B_ex = B_max + mu0 * (H_ex - H_max)
            bh_data.append([H_ex, B_ex])

        # print(bh_data)
        BHCurve = np.array(bh_data)
        # B data [Tesla]
        data_x = BHCurve[:, 1]
        # H data [A/m]
        data_y = BHCurve[:, 0]
        #inter_BHcurve = CubicSpline(data_x, data_y, bc_type='natural', extrapolate=bool)
        inter_BHcurve = interp1d(data_x, data_y, bounds_error=False, fill_value=(data_y[0], data_y[-1]))

        # B2-v 데이터 생성 --> 선형보간을 해야 맞나? 스플라인보간을 해야 맞나?
        b2v_data = list()
        for H, B in bh_data:
            if B == 0:
                v = 0
            else:
                v = H / B

            B2 = B ** 2
            b2v_data.append([B2, v])
        B2vCurve = np.array(b2v_data)
        # B2 data
        data_x = B2vCurve[:, 0]
        # v data
        data_y = B2vCurve[:, 1]
        #inter_B2vcurve = CubicSpline(data_x, data_y, bc_type='natural', extrapolate=bool)
        inter_B2vcurve = interp1d(data_x, data_y, bounds_error=False, fill_value=(data_y[0], data_y[-1]))

        return [inter_BHcurve, inter_B2vcurve]
    

    #-------------------------------------------------------------------------
    # Reference : [1], pp.171~172
    def derivatives_of_shape_functions(self, u, v, x, y, dim):
        # Calculation of shape function and their derivatives
        [N, dNdu, dNdv] = self.shape_function(u, v, dim)
        
        # Calculate of Jacobian
        #   J   = [∂x/∂u, ∂y/∂u ; ∂x/∂v, ∂y/∂v]
        #   J11 = ∑(∂Ni/∂u * xi)
        #   J12 = ∑(∂Ni/∂u * yi)
        #   J21 = ∑(∂Ni/∂v * xi)
        #   J22 = ∑(∂Ni/∂v * yi)
        J = np.zeros((2, 2))
        J[0,0] = np.sum(dNdu*x)
        J[0,1] = np.sum(dNdu*y)
        J[1,0] = np.sum(dNdv*x)
        J[1,1] = np.sum(dNdv*y)
        
        # Calcuation of det(J)
        detJ = np.linalg.det(J)
        
        # Calculation of dNdx, dNdy
        dNdx = np.zeros(dim)
        dNdy = np.zeros(dim)
        [dNdx, dNdy] = np.matmul(np.linalg.inv(J), [dNdu, dNdv])
        
        return [N, dNdu, dNdv, detJ, dNdx, dNdy]
    
    #-------------------------------------------------------------------------
    # Reference : [1], pp.172
    def shape_function(self, u, v, dim):
        # shape function
        N    = np.array([])
        
        # derivative : dN/du
        dNdu = np.array([])
        
        # derivative : dN/dv
        dNdv = np.array([])
        
        # a common value for the below calculations
        t = 1-u-v
        
        # triangular element (linear)
        if dim == 3:
            N =    np.array([t, u, v])
            dNdu = np.array([-1, 1, 0])
            dNdv = np.array([-1, 0 ,1])
        
        # quadrilateral element (bi-linear)
        elif dim == 4:
            N    = np.array([(1-u)*(1-v)/4, (1+u)*(1-v)/4, (1+u)*(1+v)/4, (1-u)*(1+v)/4])
            dNdu = np.array([(-1+v)/4, (1-v)/4, (1+v)/4, (-1-v)/4])
            dNdv = np.array([(-1+u)/4, (-1-u)/4, (1+u)/4, (1-u)/4])
        
        # triangular element (quadratic)
        elif dim == 6:
            N    = np.array([-t*(1-2*t), 4*u*t, -u*(1-2*u), 4*u*v, -v*(1-2*v), 4*v*t])
            dNdu = np.array([1-4*t, 4*(t-u), -1+4*u, 4*v, 0, -4*v])
            dNdv = np.array([1-4*t, -4*u, 0, 4*u, -1+4*v, 4*(t-v)])
        
        # quadrilateral element (quadratic)
        elif dim == 8:
            N    = np.array([-(1-u)*(1-v)*(1+u+v)/4, \
                              (1-u*u)*(1-v)/2, \
                             -(1+u)*(1-v)*(1-u+v)/4, \
                              (1+u)*(1-v*v)/2, \
                             -(1+u)*(1+v)*(1-u-v)/4, \
                              (1-u*u)*(1+v)/2,\
                             -(1-u)*(1+v)*(1+u-v)/4,\
                              (1-u)*(1-v*v)/2])
            dNdu = np.array([ (1-v)*(2*u+v)/4, \
                             -(1-v)*u, \
                              (1-v)*(2*u-v)/4, \
                              (1-v*v)/2, \
                              (1+v)*(2*u+v)/4, \
                             -(1+v)*u, \
                              (1+v)*(2*u-v)/4, \
                             -(1-v*v)/2])
            dNdv = np.array([ (1-u)*(u+2*v)/4, \
                             -(1-u*u)/2, \
                             -(1+u)(u-2*v)/4, \
                             -(1+u)*v, \
                              (1+u)*(u+2*v)/4, \
                              (1-u*u)/2, \
                             -(1-u)(u-2*v)/4, \
                             -(1-u)*v])
                     
        return [N, dNdu, dNdv]
    
    #-------------------------------------------------------------------------
    # Gauss-Legendre integration: Weights and evaluation points for integration on triangle
    # Reference : [1], pp. 173-174
    # where, n is number of integration points
    def get_integration_weight(self, dim):
        # integration points n
        n = 0
        if   dim == 3 : n = 1   # linear triangle
        elif dim == 6 : n = 3   # quadratic triangle
        elif dim == 4 : n = 4   # bi-linear quadrilateral
        elif dim == 8 : n = 4   # quadratic quadrilateral
        
        # 1차 삼각형 요소
        if n == 1:
            ui = [1/3]  # 0 < u < 1
            vi = [1/3]  # 0 < v < 1
            wi = [1/2]
            
        # 2차 삼각형 요소
        elif n == 3:
            ui = [1/6, 2/3, 1/6]  # 0 < u < 1
            vi = [1/6, 1/6, 2/3]  # 0 < v < 1
            wi = [1/6, 1/6, 1/6]
        
        # 사각형요소 (3차 다항식까지 유효함)
        elif n == 4:
            sqrt3 = math.sqrt(3)
            ui = [1/sqrt3, 1/sqrt3, -1/sqrt3, -1/sqrt3] # -1 < u < 1
            vi = [1/sqrt3, -1/sqrt3, 1/sqrt3, -1/sqrt3] # -1 < v < 1
            wi = [1, 1, 1, 1]
            
        return [ui, vi, wi]
    
    
    #-------------------------------------------------------------------------
    # 요소방정식 만들기
    def makeElementEquation(self, iter):      
        mu0 = 4 * np.pi * 1e-7
        
        #print('Number of Elements:', len(self.FE_Elements_))
        for e in self.FE_Elements_.values():
            # 요소의 재질/소스 데이터 가져오기
            regionID   = e['regionID']
            
            region     = self.FE_Regions_[regionID]
            sourceID   = region['sourceID']
            materialID = region['materialID']
            area       = region['area']
            
            # ----------------------------------------------------------------
            # 재질 설정
            # 참조하는 재질이 있으면 셋팅함
            mat = self.FE_Materials_[int(materialID)]
            
            # BH데이터 설정
            BH  = mat['BH'] # x축 B,   y축 H
            B2v = mat['B2v']# x축 B^2, y축 v

            # ----------------------------------------------------------------
            # 소스 : 자화설정
            Mx = 0
            My = 0
            Magnetization = mat['M']
            if Magnetization > 0:
                vMx, vMy = mat['dirM']
                vM = math.sqrt(vMx**2 + vMy**2)
                uMx = vMx/vM
                uMy = vMy/vM
                Mx = Magnetization * uMx
                My = Magnetization * uMy
                
                # 영역이 참조하는 좌표계를 적용한다
                headNode = bzmag.getObject(regionID)
                cs = headNode.CoordinateSystem
                if cs != None:
                    m11, m12, m13, m21, m22, m23 = cs.getTransformation()
                else:
                    m11, m12, m13, m21, m22, m23 = [1,0,0, 0,1,0]
                    
                mtx = np.array([[m11, m12, 0],
                                [m21, m22, 0],
                                [0,   0,   1]])
                refcs = Affine2D(matrix=mtx)
                Mx, My = refcs.transform((Mx, My))
                #print('Magnetization', Mx, My)
                
            # 소스 : 전류밀도 설정
            # 소스 가져오기 --> 추후 구현
            J = 0
            if sourceID in self.FE_Sources_.keys():
                src = self.FE_Sources_[sourceID]
                current = src['I']
                parallel_branches = src['ParallelBranches']
                J  = current / area / parallel_branches
                #print('Current Density', regionID, J)
            
            # ----------------------------------------------------------------
            # 요소방정식 생성
            # 요소를 이루는 절점 ID 가져오기
            nodeIDs = e['nodes']
            
            # 추후 어셈블리 시 노드 ID가 필요함
            # 노드 ID를 어셈블리 시 행렬 인덱스와 일치시킬 예정임
            # 참고) 추후 주기경계조건 처리를 해야하는 경우 주기경계 상의 노드 절점은 따로 처리되므로
            #       Master/Slave 상의 노드 중 Slave 노드가 Master 노드로 맵핑됨
            #       결국 Assembly 시 Slave 노드는 존재하지 않는 노드가 되므로 
            #       이들은 행렬 인덱스상 가장 마지막에 위치해야 편리함 (아닐지도 모르고...;;)
            # 2019.08.19   
            # 요소 형태 알아내기(절점의 수로 알아냄)
            dim = len(nodeIDs)
            
            # 이전 스텝의 벡터포텐셜 가져오기
            # 이전 연산에서의 벡터포텐셜이 self.A_에 저장되어 있음(즉, 크기가 0보다 큼)
            if iter == 0 :  
                A_prev = np.zeros(dim)
            else:
                A_prev = self.A_[nodeIDs]
                    
            # 소스항 0으로 초기화
            Q = np.zeros(dim)
            
            # 요소절점의 좌표 얻기
            x = np.zeros(dim)
            y = np.zeros(dim)
            for i, nodeID in enumerate(nodeIDs):
                # 노드 ID를 얻고
                node = self.FE_Nodes_[nodeID]
                
                # 요소 노드 좌표 (단위 변환을 위해 1e-3 곱해줌; mm->m)
                x[i], y[i] = node['point']*1e-3
                
            # 겔러킨법의 적용 (요소방정식)
            # 미분방정식 : "div(v gradA) + J = 0"
            # 여기서 v는 상수 (v는 텐서일 수 있음 -> 이 경우 수식전개 동일한지 고려해 봐야함)
            #        A는 미지수(자기벡터 포텐셜)
            #        J는 소스 항 (전류밀도)
            # 상기 미분방정식에 겔러킨법 적용함 (가중함수를 이용한 적분이 필요함)
            # 1) 정식화 (예, 첫번째 항)
            #    ∫_global{grad(Nt)·v grad(N)A} dxdy
            #    = ∫_local{grad(Nt)·v grad(N)A det(J)} dudv
            # 2) 수치적분 (가우스 르장드르 적분 적용)
            #    ∫(K(u)) du = ∑wi K(ui)
            SS  = np.zeros((dim,dim)) # stiffiness matrix
            SS1 = np.zeros((dim,dim)) # stiffiness matrix sub1 ; 본래 계수행렬
            SS2 = np.zeros((dim,dim)) # stiffiness matrix sub2 ; NR 법을 위한 미분항 계수행렬
            
            # get integration points and corresponding weights
            [ui, vi, wi] = self.get_integration_weight(dim)
                
            # do integration
            for i, w in enumerate(wi): 
                u = ui[i]
                v = vi[i]
                [N, dNdu, dNdv, detJ, dNdx, dNdy] = self.derivatives_of_shape_functions(u, v, x, y, dim)
                
                Nt = N.T
                gradN = np.array([dNdx, dNdy])
                gradNt = gradN.T
                
                # Flux density 
                # [Bx, By] = Curl(NA)
                Bx =  np.matmul(dNdy, A_prev)
                By = -np.matmul(dNdx, A_prev)
                B2 =  Bx**2 + By**2
                absB = np.sqrt(B2)
                
                # find reluctivity (1/mu)
                # 실제로 이방성 재질을 고려하게끔 수식을 유도했으나,
                # 프로그램에서 아직 이방성 재질을 설정할 수 있도록 만들어지지 않아
                # 이방성 재질에 대한 수식을 사용하되 nur_x 와 nur_y 가 같게 설정한다
                # --> 즉 등방성 재질을 사용한 경우의 해석이다
                if BH != None:
                    # B가 0인 경우에는 H도 0이기 때문에 H/B로 nu를 구할수 없다
                    if absB == 0: nu = BH(0.00001) / 0.00001
                    else : nu = BH(absB) / absB
                else :
                    nu = 1 / (mat['mur']*mu0)
                
                SS1 = SS1 + np.matmul(gradNt, nu*gradN) * detJ*w
                
                #--------------------------------------------------
                # source term (Current Density and Magnetization)
                M = np.array([nu*My, -nu*Mx])       # 자화벡터 ; 좌측 y, x 순서 및 부호 주의!
                
                # 소스항 벡터
                Q = Q + (Nt*J - np.matmul(gradNt, M)) * detJ*w
                
                
                #--------------------------------------------------
                # 비선형 해석을 위해서는 dv/dB2 이 필요함
                # 선형 재질일때는 dv/dB2 = 0이다
                dvdB2 = 0
                if iter > 0:
                    if B2v != None:
                        dvdB2 = fp(B2v, B2)
                        
                        # v-B2 그래프는 단조증가함수이어야 한다! 해의 수렴을 위하여~!
                        if dvdB2 < 0:
                            print('dvdB2 should be positive value!!, B:', Bx, By, B2)
                            dvdB2 = 0
                            
                    Ex = -dNdx*By
                    Ey =  dNdy*Bx
                    
                    # 하기는 등방성 재질 고려한 SS2 구하기
                    E = Ex + Ey
                    ff = np.outer(E, E.T)
                    SS2 = SS2 + ((2*dvdB2*ff)* detJ*w)
                    
                    
                else:
                    SS2 = np.zeros((dim, dim))
                
            
            # jacobian matrix, forcing vector
            SS = SS1 + SS2
            
            # the component of the right-hand side depending on the potentials
            KA = SS1.dot(A_prev)
            Q = Q - KA
            
            # 요소 속성 저장
            e['dim'] = dim
            e['J']   = J
            e['nu']  = [nu, nu]
            e['M']   = [Mx, My]
            e['BH']  = BH
            e['B2v'] = B2v
            e['D']   = detJ
            
            # 요소방정식 계수행렬 및 소스 벡터 저장
            e['SS']  = SS
            e['SS1'] = SS1
            e['SS2'] = SS2
            e['Q']   = Q

        return
        
    #-------------------------------------------------------------------------
    def solve(self):
        #self.A_ = np.array([])
        
        iter = 0
        while True:
            print('------------------------------------')
            print('  Generating the Element Matrices   ')
            print('------------------------------------')
            self.makeElementEquation(iter)
            
            print('------------------------------------')
            print('    Assembing the Global Matrix     ')
            print('------------------------------------')
            self.matrixAssembly()
            #self.matrixAssembly2()
            
            print('------------------------------------')
            print('     Applying Bounday Conditions    ')
            print('------------------------------------')
            self.applyBoundaryConditions()
            #self.applyBoundaryConditions2()
            
            print('------------------------------------')
            print('     Solving the System Matrix      ')
            print('------------------------------------')
            dA = spsolve(self.K_, self.J_, 'NATURAL', True)
            #dA = spsolve(self.K_, self.J_)
            
            #KK = self.K_.todense()
            #bb = np.allclose(KK, self.K2_)
            #print(bb)
            
            
            
            # 최초의 풀이면
            if iter == 0:
                self.A_ = dA
                self.err_base = np.linalg.norm(dA)
                print('  err_base', self.err_base)
                print('------------------------------------')
                #break
                
            # 반복 중이면
            else:
                norm  = np.linalg.norm(dA)
                err = norm / self.err_base
                print('  Iteration:', iter, ', Error:', err)
                print('------------------------------------')
                
                self.A_ = self.A_ + dA
                if (err<0.001) or (iter>0):
                #if (iter>1):
                    break
                    
            iter = iter + 1
        return self.A_
        
    #-------------------------------------------------------------------------
    # 요소방정식을 시스템 방정식으로 어셈블리
    # 예) 삼각형요소에는 노드3개 존재(로컬 노드번호:1~3존재) --> Ne1, Ne2, Ne3   
    #     이를 이용한 계수 메트릭스 --> [Ke11, Ke12, Ke13 ; Ke21, Ke22, Ke23; Ke31, Ke32, Ke33]
    #     시스템 방정식은 삼각형 노드의 글로벌 아이디(인덱스)로 구성됨
    #     요소아이디(1~3) Ne1, Ne2, Ne3 글로벌 아이디 N1, N19, M14에 대응된다면
    #     Ke11 --> K[1][1], Ke12 --> K[1][19], Ke13 --> K[1][14] ... 와 같이 대응됨
    #     또한 다른요소에 의해서도 동일한 글로벌 요소행렬 위치를 가질수 있음
    #     즉, K[1][19] 는 위의 예에 보인 요소에 의해 만들어지는 값이기도 하지만 다른 요소에 의해 값이 만들어지기도 함
    #     이를 합치는 과정이 Assembly 과정임
    def matrixAssembly(self):
        idxRow = []
        idxCol = []
        data   = []
        
        idxSourceRow = []
        idxSourceCol = []
        source       = []
        
        # Step1. 각 요소벌 글로벌 행,렬 인덱스 저장 (idxRow, idxCol)
        #        해당 행,렬에 해당하는 값 저장하기 (data)
        for e in self.FE_Elements_.values():
            dim     = e['dim']
            nodeIDs = e['nodes']
            SS      = e['SS']
            Q       = e['Q']
            for i in range(dim):
                nRow = nodeIDs[i]
                idxSourceRow.append(nRow) 
                idxSourceCol.append(0)
                source.append(Q[i])

                for j in range(dim):
                    nCol = nodeIDs[j]
                    idxRow.append(nRow)
                    idxCol.append(nCol)
                    data.append(SS[i,j])
                    
        # Step.2 희소행렬을 만들기, csr_matrix 라이브러리 활용!
        #        csr_matrix 행렬 생성법을 찾아보면 도움됨
        #        행렬의 같은 위치에 존재하는 항은 모두 더해짐
        self.K_ = csr_matrix((data,(idxRow,idxCol)), dtype=float)
        self.J_ = csr_matrix((source,(idxSourceRow,idxSourceCol)), dtype=float)
        
    #-------------------------------------------------------------------------
    def applyBoundaryConditions(self):
        # 1)고정경계조건의 경우 (Ax = b)
        #   1) 노드번호(n)에 해당하는 행(A행렬의 n행)은 대각성분을 제외하고 모두 0 처리함
        #      대각성분은 1로 처리
        #   2) 노드번호(n)에 해당하는 소스항 (b벡터의 n항)을 주어진 고정경계값으로 처리함
        # 2) 주기경계조건의 경우
        #   ...??
        # 3) 자연경계조건의 경우               
        
        rows, cols = self.K_.nonzero()
        data = self.K_.data
        
        rows_, cols_, data_ = [], [], []
        for r, c, d in zip(rows, cols, data):
            for BCValue, nodeIDs in self.BC_Nodes_.items():
                if (r in nodeIDs) or (c in nodeIDs):
                    if r != c:
                        break
                    else:
                        rows_.append(r)
                        cols_.append(c)
                        data_.append(1)
                        self.J_[r,0] = 0
                        break
                else:
                    rows_.append(r)
                    cols_.append(c)
                    data_.append(d)
                    break

        #print(self.K_)
        self.K_ = csr_matrix((data_, (rows_, cols_)), shape=self.K_.shape)
        #print(self.K_)
        
        
    #-------------------------------------------------------------------------
    # 희소행렬을 사용하지 않고 시스템행렬을 꾸밈
    def solve2(self):
        iter = 0
        while True:
            print('------------------------------------')
            print('  Generating the Element Matrices   ')
            print('------------------------------------')
            self.makeElementEquation(iter)
            
            print('------------------------------------')
            print('    Assembing the Global Matrix 2   ')
            print('------------------------------------')
            self.matrixAssembly2()
            
            print('------------------------------------')
            print('    Applying Bounday Conditions 2   ')
            print('------------------------------------')
            self.applyBoundaryConditions2()
            
            print('------------------------------------')
            print('     Solving the System Matrix 2    ')
            print('------------------------------------')
            dA = np.linalg.solve(self.K2_, self.J2_)
            #close = np.allclose(self.K2_.dot(dA), self.J2_)
            #print(close)
            
            if iter == 0:
                self.A_ = dA
                self.err_base = np.linalg.norm(dA)
                print('  err_base', self.err_base)
                print('------------------------------------')
            else:
                err = np.linalg.norm(dA) / self.err_base
                print('Iteration:', iter, ', Error:', err)
                print('------------------------------------')
                self.A_ = self.A_ + dA
                if (err<0.001) or (iter>5):
                    break

            iter = iter + 1
        return self.A_
        
        
    #-------------------------------------------------------------------------
    # 희소행렬을 사용하지 않고 시스템행렬을 꾸밈
    def matrixAssembly2(self):
        nSize = len(self.FE_Nodes_)
        self.K2_ = np.zeros((nSize, nSize))
        self.J2_ = np.zeros(nSize)
        
        
        for e in self.FE_Elements_.values():
            dim     = e['dim']
            nodeIDs = e['nodes']
            SS      = e['SS']
            Q       = e['Q']
            for i in range(dim):
                nRow = nodeIDs[i]
                self.J2_[nRow] = self.J2_[nRow] + Q[i]

                for j in range(dim):
                    nCol = nodeIDs[j]
                    self.K2_[nRow,nCol] = self.K2_[nRow,nCol] + SS[i,j]
                    
                    
    #-------------------------------------------------------------------------
    def applyBoundaryConditions2(self):
        # 1)고정경계조건의 경우 (Ax = b)
        #   1) 노드번호(n)에 해당하는 행(A행렬의 n행)은 대각성분을 제외하고 모두 0 처리함
        #      대각성분은 1로 처리
        #   2) 노드번호(n)에 해당하는 소스항 (b벡터의 n항)을 주어진 고정경계값으로 처리함
        # 2) 주기경계조건의 경우
        #   ...??
        # 3) 자연경계조건의 경우 
        for key, values in self.BC_Nodes_.items():
            for nodeID in values:
                self.K2_[:, nodeID] = 0
                self.K2_[nodeID, :] = 0
                self.K2_[nodeID, nodeID] = 1
                self.J2_[nodeID] = 0
        
        
    #-------------------------------------------------------------------------
    # 가우스 적분의 사용
    def calculateFluxdensity(self):
        e_size = len(self.FE_Elements_)
        if e_size == 0: return 
        
        self.B_ = np.zeros((e_size,2))
        for key, e in self.FE_Elements_.items():
            # get integration points and corresponding weights
            dim     = e['dim']
            nodeIDs = e['nodes']
            [ui, vi, wi] = self.get_integration_weight(dim)
            
            A = np.zeros(dim)
            x = np.zeros(dim)
            y = np.zeros(dim)
            for i, nodeID in enumerate(nodeIDs):
                A[i] = self.A_[nodeID]
                node = self.FE_Nodes_[nodeID]
                
                # 요소 노드 좌표 (단위 변환을 위해 1e-3 곱해줌; mm->m)
                x[i], y[i] = node['point']*1e-3
                
            Bx = np.zeros(len(wi))
            By = np.zeros(len(wi))
            
            # do integration
            for i, w in enumerate(wi): 
                u = ui[i]
                v = vi[i]
                [N, dNdu, dNdv, detJ, dNdx, dNdy] = self.derivatives_of_shape_functions(u, v, x, y, dim)
                
                # Flux density 
                # [Bx, By] = Curl(NA)
                Bx[i] =  np.matmul(dNdy, A)
                By[i] = -np.matmul(dNdx, A)

            Bxm = np.mean(Bx, axis=0)
            Bym = np.mean(By, axis=0)
            e['B'] = [Bxm, Bym]
            #if key == 255:
            #    print('flux density of 255: ',Bx, By, Bxm, Bym)
            self.B_[key] = [Bxm, Bym]
        
        return self.B_
        
        
    #-------------------------------------------------------------------------
    # 가우스적분을 사용하지 않고 적분결과식을 이용함
    def calculate_fluxdensity(self):
        e_size = len(self.FE_Elements_)
        if e_size == 0: return 
        
        self.B_ = np.zeros((e_size,2))
        for key, e in self.FE_Elements_.items():
            # get integration points and corresponding weights
            dim     = e['dim']
            nodeIDs = e['nodes']
            [ui, vi, wi] = self.get_integration_weight(dim)
            
            Ae = np.zeros(dim)
            x = np.zeros(dim)
            y = np.zeros(dim)
            for i, nodeID in enumerate(nodeIDs):
                Ae[i] = self.A_[nodeID]
                node = self.FE_Nodes_[nodeID]
                
                # 요소 노드 좌표 (단위 변환을 위해 1e-3 곱해줌; mm->m)
                x[i], y[i] = node['point']*1e-3

            # 요소면적
            delta = (x[0] * y[1] + x[1] * y[2] + x[2] * y[0] - (y[0] * x[1] + y[1] * x[2] + y[2] * x[0])) / 2.0

            # 계수 행렬을 위한 계수들 계산
            b = np.zeros(3)
            b[0] = y[1] - y[2]
            b[1] = y[2] - y[0]
            b[2] = y[0] - y[1]

            c = np.zeros(3)
            c[0] = x[2] - x[1]
            c[1] = x[0] - x[2]
            c[2] = x[1] - x[0]

            By = -b.dot(Ae) / (2 * delta)
            Bx = c.dot(Ae) / (2 * delta)
            
            self.B_[key] = [Bx, By]

        return self.B_
        
    #-------------------------------------------------------------------------
    def calculate_fluxlinkage(self):
        # self.FE_Sources_[nodeID] = {'I' : current, 'ParallelBranches' : a, 'Coil' : coil_info}

            
        for key, value in self.FE_Sources_.items():
            # coil_info.append([head, turns, direction])
            coils = value['Coil']
            flux_linkage = 0
            for coil in coils:
                # [head, turns, direction]
                head = coil[0]
                turns = coil[1]
                direction = coil[2]
                regionID = head.getID()
                
                #self.FE_Regions_[regionID] = {'sourceID' : -1, 'materialID' : materialID, 'area' : area, 'elements' : elementIDs}
                region = self.FE_Regions_[regionID]
                area   = region['area']
                eIDs   = region['elements']
                
                if direction :
                    coil_linkage = self.integration(eIDs, area, turns)
                else:
                    coil_linkage = -self.integration(eIDs, area, turns)
                    
                    
                print('coil side linkage', coil_linkage)
                
                '''
                #print(eIDs)
                coil_nodeIDs = np.array([], dtype=int)
                for eID in eIDs:
                    #print('eID', eID)
                    e = self.FE_Elements_[eID]
                    nodeIDs = e['nodes']
                    coil_nodeIDs = np.append(coil_nodeIDs, nodeIDs)
                
                coil_nodeIDs = np.unique(coil_nodeIDs)
                #print('Coil NodeID:', coil_nodeIDs)
                ff = self.A_[coil_nodeIDs]
                flux = ff.sum() * turns
                if direction == True:
                    flux_linkage = flux_linkage + flux
                else:
                    flux_linkage = flux_linkage - flux
                '''
                
                flux_linkage = flux_linkage + coil_linkage 
                
            print('flux linkage', flux_linkage)
        
    #-------------------------------------------------------------------------
    def integration(self, eIDs, area, turns):
        K = turns / area * 1e6
        flux_linkage = 0
        for eID in eIDs:
            e = self.FE_Elements_[eID]
            nodeIDs = e['nodes']
            dim = nodeIDs.shape[0]
            
            #print('중간테스트, 노드아이디',nodeIDs, dim)
            # get integration points and corresponding weights
            [ui, vi, wi] = self.get_integration_weight(dim)
            
            x = np.zeros(dim)
            y = np.zeros(dim)
            Q = np.zeros(dim)
            for i, nodeID in enumerate(nodeIDs):
                # 노드 ID를 얻고
                node = self.FE_Nodes_[nodeID]
                
                # 요소 노드 좌표 (단위 변환을 위해 1e-3 곱해줌; mm->m)
                x[i], y[i] = node['point']*1e-3
            
            # do integration
            for i, w in enumerate(wi): 
                u = ui[i]
                v = vi[i]
                [N, dNdu, dNdv, detJ, dNdx, dNdy] = self.derivatives_of_shape_functions(u, v, x, y, dim)
                
                Nt = N.T
                Q = Q + (Nt*K) * detJ*w
        
            A = self.A_[nodeIDs]
            flux_linkage = flux_linkage + Q.dot(A)
        
        return flux_linkage
        
# References
# [1] Joao Pedro A. Bastos, "Electromagnetic Modeling by Finite Element Methods"
# [2] Kay Hameyer, Ronnie Belmans, "Numerical Modelling and Design of Electrical Machines and Devices"
# [3] P.P. Silvester & R.L.Ferrari, "Finiete elements for electrical engineers, 2nd Ed."
