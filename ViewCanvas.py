from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import (
    QRect, 
    QSize, 
    Qt, 
    pyqtSignal, 
    pyqtSlot)
    
from matplotlib import rcParams
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from pyqtcore import (QList)

import matplotlib.cm as cm
import numpy as np
import bzmagPy as bzmag
import matplotlib.pyplot as plt

#from lib.SurfaceArtist import SurfaceArtist
#from lib.CSArtist import CSArtist
from lib.FluxlineArtist import FluxlineArtist
from lib.MeshArtist import MeshArtist
from lib.CSArtist_new import CSArtist_new
from lib.SurfaceArtist_new import SurfaceArtist_new
from lib.EdgeArtist import EdgeArtist
from lib.BCArtist import BCArtist


class ViewCanvas(FigureCanvas):
    itemsSelected = pyqtSignal(QList)
    itemUnSelectAll = pyqtSignal()
    _xmin = 0
    _xmax = 0
    _ymin = 0
    _ymax = 0
    
    # 요소 Aritst
    mesh_ = None
    
    # 포텐셜
    pot_ = None
    
    # 플럭스라인 Artist
    fluxline_ = None
    
    # 플럭스 컨투어 Artist
    contour_ = None
    
    # 자속밀도
    tpc_ = None
    tpc_bar_ = None
    
    # ------------------------------------------------------------------------
    def __init__(self, parent=None, figpx=None, figsize=None, dpi=None):
        # If no figsize is given, use figure.figsize from matplotlibrc
        if figsize is None:
            figsize=rcParams['figure.figsize']
        
        # If no dpi is given, use figure.dpi from matplotlibrc
        if dpi is None:
            dpi=rcParams['figure.dpi']
        
        # When given, figpx overrides figsize. 
        # figsize is calculated from figpx and dpi. 
        if figpx is not None:
            figsize=(figpx[0]*1.0/dpi, figpx[1]*1.0/dpi)
            pxsize=figpx
        else:
            pxsize=figsize[0]*dpi,figsize[1]*dpi
        
        self.userpxsize=pxsize
        
        self.figureObject=Figure(figsize=figsize, dpi=dpi)
        self.figureObject.set_figheight(figsize[1])
        self.figureObject.set_figwidth(figsize[0])
        super(ViewCanvas, self).__init__(self.figureObject)

        self.setParent(parent)
        self.figureObject.set_edgecolor('black')
        self.figureObject.set_facecolor('white')
        self.figureObject.tight_layout()
        
        # fig를 1행 1칸으로 나누어 1칸안에 넣어줍니다
        # left, bottom, width, height; 모든 값은 1미만이어야 한다
        ax = self.figureObject.add_axes([0,0,1,1])
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')
        ax.spines['bottom'].set_position(('data', 0))
        ax.spines['left'].set_position(('data', 0))
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')
        
        #ax.set_aspect('equal', 'box', 'C', True)
        ax.set_aspect('equal', 'datalim', 'C', True)
        #ax.grid(color='r', linestyle='-')
        ax.autoscale_view()
        
        
        # axes 설정
        self.fig_ = self.figureObject
        self.axes_ = ax;
        
        # Selected GeomNodes
        self.selectedNodeIDs_ = QList()
        
        # GeomHeadNodeID to ArtistItem
        self.HeadNodeToItem_ = {}
        
        # CSNodeID to ArtistItem
        self.CSNodeToItem_ = {}
        
        # GeomNodeID to its csID
        self.GeomNodeToReferedCS_ = {}
        
        # BCNodeID to ArtistItem
        self.BCNodeToItem_ = {}
        
        # Connect matplotlib event handlers
        self.figureObject.canvas.mpl_connect('pick_event', self.on_pick_event)
        self.figureObject.canvas.mpl_connect('resize_event', self.on_size_event)
        
        # 선택모드; 0: 아무것도 안함, 1:오브젝트, 2: 엣지
        self.selectMode_ = 1
        
        #print(ax.properties())
        
        
    # ------------------------------------------------------------------------
    def visible_mesh(self, visible):
        if self.mesh_ == None: return
        if visible == True : 
            for tri in self.mesh_:
                tri.set_visible(True)
        else :
            for tri in self.mesh_:
                tri.set_visible(False)
        self.draw()
    
    # ------------------------------------------------------------------------
    def visible_fluxline(self, visible):
        if self.fluxline_ == None: return
        if visible == True : 
            for flux in self.fluxline_.collections:
                flux.set_visible(True)
            #for cont in self.contour_.collections:
            #    cont.set_visible(True)
        else :
            for flux in self.fluxline_.collections:
                flux.set_visible(False)
            #for cont in self.contour_.collections:
            #    cont.set_visible(False)
        self.draw()
    
    # ------------------------------------------------------------------------
    def visible_fluxdensity(self, visible):
        if self.tpc_ == None: return
        if visible == True : 
            self.tpc_.set_visible(True)
            tcp_pos=self.figureObject.add_axes([0.93,0.25,0.02,0.5])  ## the parameters are the specified position you set 
            self.tpc_bar_ = self.figureObject.colorbar(self.tpc_, cax=tcp_pos)
        else :
            self.tpc_.set_visible(False)
            self.tpc_bar_.remove()
            
        self.draw()
        
    # ------------------------------------------------------------------------
    def setSelectMode(self, mode):
        self.selectMode_ = mode
    
    
    # ------------------------------------------------------------------------
    def setMeshData(self, mesh):
        # 기존 데이터 삭제
        if self.mesh_ != None:
            for tri in self.mesh_:
                tri.remove()

        if not 'vertices' in mesh:
            return
        if not 'triangles' in mesh:
            return
            
        vets = mesh['vertices']
        triangles = mesh['triangles']
        xs = vets[:, 0]
        ys = vets[:, 1]

        axes = self.axes_
        self.mesh_ = axes.triplot(xs, ys, triangles, 'g-', lw=0.1, antialiased=False)
        #print(self.axes_.viewLim, self.axes_.viewLim)
        
        self.draw()
    
    # ------------------------------------------------------------------------
    def setPontentials(self, mesh, pot):
        # 메시정보가 설정되어야 포텐셜 설정가능함
        if self.mesh_ == None: return
        
        vets = mesh['vertices']
        triangles = mesh['triangles']
        xs = vets[:, 0]
        ys = vets[:, 1]
        
        # 노드수와 포텐셜수는 같아야 함
        if (len(xs) != len(ys)) or (len(xs) != len(pot)):
            print('Number of nodes and potential is not the same!!')
            return 
        
        if self.fluxline_ != None:
            for flux in self.fluxline_.collections:
                flux.remove()
        if self.contour_ != None:
            for flux in self.contour_.collections:
                flux.remove()
        

        axes = self.axes_
        cmap = cm.get_cmap(name='rainbow', lut=None)
        self.fluxline_ = axes.tricontour(xs, ys, triangles, pot, levels=20, linewidths=0.2, colors='k')
        #self.contour_  = axes.tricontourf(xs, ys, triangles, pot, levels=20, cmap="RdBu_r")
        self.draw()
             
        
# ------------------------------------------------------------------------
    def setFluxdensity(self, mesh, B):
        # 메시정보가 설정되어야 포텐셜 설정가능함
        if self.mesh_ == None: return
        
        vets = mesh['vertices']
        triangles = mesh['triangles']
        xs = vets[:, 0]
        ys = vets[:, 1]
        
        Bx = B[:, 0]
        By = B[:, 1]
        absB = np.sqrt(Bx * Bx + By * By)
        
        axes = self.axes_
        cmap = cm.get_cmap(name='rainbow', lut=None)

        if self.tpc_bar_ != None:
            self.tpc_bar_.ax.clear()
            self.tpc_bar_.remove()
            
        if self.tpc_ != None:
            self.tpc_.remove()
            
        self.tpc_ = axes.tripcolor(xs, ys, triangles, facecolors=absB, shading='flat', cmap=cmap)
        
        tcp_pos=self.figureObject.add_axes([0.93,0.25,0.02,0.5])  ## the parameters are the specified position you set 
        self.tpc_bar_ = self.figureObject.colorbar(self.tpc_, cax=tcp_pos)
        
        self.draw()
        
        
    # User Slots--------------------------------------------------------------
    # Signal Generated from NOHTree
    @pyqtSlot(int)
    def addItem(self, nodeID):
        #print("Add Item in the ViewCanvas")
        node = bzmag.getNode(nodeID)
        
        # make a map for GeomBaseNode to its refered CS ID
        if 'GeomBaseNode' in node.getGenerations():
            cs = node.CoordinateSystem
            csID = -1
            if cs != None: csID = cs.getID()
            self.GeomNodeToReferedCS_[nodeID] = csID
        
        # make a GraphicItem for the HeadNode and
        # make a map for GeomHeadNode ID to GraphicItem
        if 'GeomHeadNode' == node.getTypeName() and node.IsStandAlone:
            print('Object is Added')
            item = SurfaceArtist_new(node)
            nEdge = node.getNumberOfEdge()
            self.HeadNodeToItem_[nodeID] = {'head': item, 'num_edges': nEdge}
            self.axes_.add_artist(item)

            # Edge 추가
            for idx in range(nEdge):
                edge = EdgeArtist(node, idx)
                self.HeadNodeToItem_[nodeID].update({'edge_' + str(idx): edge})
                self.axes_.add_artist(edge)
            
            # axes의 limit 설정
            self.update_axes_limit(item)
            
        # make a map for CSNode ID to GraphicItem
        if 'CSNode' == node.getTypeName():
            print('CS is Added')
            
            #item = CSArtist(self, self.axes_)
            item = CSArtist_new(self.axes_, node, 1.0, 1.0, 0.1)
            self.CSNodeToItem_[nodeID] = item
            self.axes_.add_artist(item)
            item.set_visible(False)
            
        # make a map for BCNode ID to GraphicItem
        if 'BCNode' in node.getGenerations():
            item = BCArtist(node)
            self.BCNodeToItem_[nodeID] = item
            self.axes_.add_artist(item)
            item.set_visible(False)
        
        #print(self.axes_.viewLim, self.axes_.viewLim)
        self.auto_zorder()
        self.draw()
        
    # ------------------------------------------------------------------------
    # View/Hide of CS Item related to the bznode
    # Signal Generated from NOHTree
    @pyqtSlot(QList)
    def nodesSelected(self, nodeIDs):
        # Previous selected items are removed from the scene
        #print('View Widget, Selected Nodes:', nodeIDs, self.selectedNodeIDs_)
        for nodeID in self.selectedNodeIDs_:
            node = bzmag.getObject(nodeID)
                
            if 'GeomBaseNode' in node.getGenerations():
                csID = self.GeomNodeToReferedCS_[nodeID]
                if csID != -1:
                    item = self.CSNodeToItem_[csID]
                    item.set_visible(False)
            
            if 'GeomHeadNode' == node.getTypeName() and node.IsStandAlone:
                item = self.HeadNodeToItem_[nodeID]['head']
                item.setSelected(False)
                
            if 'CSNode' == node.getTypeName():
                item = self.CSNodeToItem_[nodeID]
                item.set_visible(False)
            
            if 'BCNode' in node.getGenerations():
                item = self.BCNodeToItem_[nodeID]
                item.set_visible(False)
                item.setSelectedStatus(False)
                
        # Current selected items are added to the scene
        for nodeID in nodeIDs:
            node = bzmag.getObject(nodeID)
            
            if 'GeomBaseNode' in node.getGenerations():
                csID = self.GeomNodeToReferedCS_[nodeID]
                if csID != -1:
                    item = self.CSNodeToItem_[csID]
                    item.set_visible(True)
                    
            if 'GeomHeadNode' == node.getTypeName() and node.IsStandAlone:
                item = self.HeadNodeToItem_[nodeID]['head']
                item.setSelected(True)
                
            if 'CSNode' == node.getTypeName():
                item = self.CSNodeToItem_[nodeID]
                item.set_visible(True)
                
            if 'BCNode' in node.getGenerations():
                item = self.BCNodeToItem_[nodeID]
                item.set_visible(True)
                item.setSelectedStatus(True)
        
        self.selectedNodeIDs_ = nodeIDs
        self.draw()
        
    # ------------------------------------------------------------------------
    # update Graphic Item with related GeomHead node
    # Signal Generated from PropertyWidget
    @pyqtSlot(int)
    def updateItem(self, nodeID, showCS=True):
        #print('View Widget, Update Node:', nodeID)
        if nodeID == -1:
            return
        
        # get bzmag Node Object by its ID
        node = bzmag.getObject(nodeID)
        
        # Update GeomNode and related GeomNodes
        if 'GeomBaseNode' in node.getGenerations():
            self.updateGeomBaseNodeItem(nodeID, showCS)
            
        # Update CS node and related GeomNodes
        if 'CSNode' == node.getTypeName():
            self.updateCSNodeItem(nodeID, showCS)
        
        # Update CS node and related GeomNodes
        if 'BCNode' in node.getGenerations():
            self.updateBCNodeItem(nodeID)
            
        self.draw()
    
    # ------------------------------------------------------------------------
    def updateGeomBaseNodeItem(self, nodeID, showCS):
        # Hide CS Artist of the previous node from the canvas 
        prev_csID = self.GeomNodeToReferedCS_[nodeID]
        if prev_csID != -1 and showCS:
            item = self.CSNodeToItem_[prev_csID]
            item.set_visible(False)
        
        node = bzmag.getObject(nodeID)
        #print('Updated', node.name)
        
        
        # Update referred CoordinateSystem of the GeomNode
        csID = -1
        cs = node.CoordinateSystem
        if cs != None: csID = cs.getID()
        self.GeomNodeToReferedCS_[nodeID] = csID
        if csID != -1 and showCS:
            item = self.CSNodeToItem_[csID]
            item.set_visible(True)
        
        # Find HeadNode
        if 'GeomHeadNode' == node.getTypeName(): hn = node
        else: hn = node.getHeadNode()
        
        if hn == None: return
            
        # When the HeadNode is not standalone node
        # ex) it is referred by boolean node
        if hn.IsStandAlone == False:
            head = hn.getHeadNode()
            if head != None :
                #print('head', hn.getID(), head.getID())
                self.updateGeomBaseNodeItem(head.getID(), False)

        # Update Top Level HeadNode
        else:
            headID = hn.getID()
            item = self.HeadNodeToItem_[headID]['head']
            
            # Artist 내용 업데이트 ; setNode() 매써드를 이용해 업데이트 함
            if item != None: item.setNode(hn)
            
            # axes의 limit 설정
            #print('reset axes limit')
            self.update_axes_limit(item)
            
            if hn.IsHide == True: 
                item.set_visible(False)
            else: 
                item.set_visible(True)
            
            # Edge 업데이트
            # 기존 엣지 지우기
            num_edges = self.HeadNodeToItem_[headID]['num_edges']
            for idx in range(num_edges):
                edge = self.HeadNodeToItem_[headID]['edge_' + str(idx)]
                edge.remove()
                
            # 새로운 엣지 추가
            nEdge = hn.getNumberOfEdge()
            self.HeadNodeToItem_[headID]['num_edges'] = nEdge
            for idx in range(nEdge):
                edge = EdgeArtist(hn, idx)
                self.HeadNodeToItem_[headID].update({'edge_' + str(idx): edge})
                self.axes_.add_artist(edge)
                if hn.IsHide == True: 
                    edge.set_visible(False)
                    edge.disablePicker()
                else:
                    edge.set_visible(True)
                    edge.enablePicker()
                    
        # Update CloneFromNode
        while node != None: 
            n = None
            for n in node.getChildren():
                if n.getTypeName() == 'GeomCloneToNode':
                    for o in n.getClonedNodes():
                        parent = o.getParent()
                        self.updateItem(parent.getID(), False)
                    
            node = n  
            
    # ------------------------------------------------------------------------        
    def updateCSNodeItem(self, nodeID, showCS):
        node = bzmag.getObject(nodeID)
        item = self.CSNodeToItem_[nodeID]
        item.setNode(node)
        
        for geomID, csID in self.GeomNodeToReferedCS_.items():
            if csID == nodeID:
                self.updateItem(geomID, showCS)

    # ------------------------------------------------------------------------        
    def updateBCNodeItem(self, nodeID):
        node = bzmag.getObject(nodeID)
        item = self.BCNodeToItem_[nodeID]
        item.setEdge(node)

        
    # ------------------------------------------------------------------------
    def update_axes_limit(self, item):
        xmin, xmax, ymin, ymax = item.boundingRect()
        
        if xmin < self._xmin : self._xmin = xmin
        if ymin < self._ymin : self._ymin = ymin
        if xmax > self._xmax : self._xmax = xmax
        if ymax > self._ymax : self._ymax = ymax
        self._xmin = self._xmin*1.05
        self._ymin = self._ymin*1.05
        self._xmax = self._xmax*1.05
        self._ymax = self._ymax*1.05
        
        #print(xmin, xmax, ymin, ymax)
        #self.axes_.set_xlim(left=self._xmin, right=self._xmax, auto=True)
        #self.axes_.set_ylim(bottom=self._ymin, top=self._ymax, auto=True)
        #self.axes_.set_xbound(self._xmin, self._xmax)
        #self.axes_.set_ybound(self._ymin, self._ymax)
        #print(self.axes_.get_xlim())
        
    # ------------------------------------------------------------------------
    # zorder 설정
    def auto_zorder(self):
        headlist = list()
        zorderlist = list()
        for key, val in self.HeadNodeToItem_.items():
            item = val['head']
            headlist.append(item)
        
        if len(headlist) == 0: return
        
        item = headlist[0]
        zorderlist.append(item)
        for i in range(1, len(headlist)):
            s1 = headlist[i].getNode()
            for j in range(i+1, len(headlist)):
                item = headlist[j]
                s2 = item.getNode()
                r = s2.contain(s1)
                if r == False:
                    zorderlist.append(item)
                else:
                    zorderlist.insert(0, item)
                
        for zorder, item in enumerate(zorderlist):
            item.set_zorder(zorder)
        
        self.draw()
        return True
            
    # ------------------------------------------------------------------------
    def on_pick_event(self, event):
        artist = event.artist
        node = artist.getNode()
        nodeID = node.getID()
        selectMode = artist.getType()
        zorder = artist.get_zorder()
        
        #print('on pick event', node.name, self.selectMode_, selectMode)
        
        if self.selectMode_ == selectMode:
            if selectMode == 1:
                print('Object select mode : nodeID=', nodeID, ', zorder=', zorder)
                nodeIDs = QList([nodeID])
                self.itemsSelected.emit(nodeIDs)
                
            elif selectMode == 2:
                edgeID = artist.getEdgeID()
                artist.setSelectedStatus(True)
                print('Edge select mode', nodeID, edgeID)
        #self.draw()

    # ------------------------------------------------------------------------
    def on_size_event(self, event):
        #print(event.width, event.height)
        width = event.width
        height = event.height
        if width < 1 : width = 1
        if height < 1 : height = 1
        
        mRatio = (self._xmax - self._xmin) / (self._ymax - self._ymin)
        wRatio = width / height
        aRatio = max(mRatio, wRatio)
        
        if mRatio > wRatio:
            xlim = [self._xmin, self._xmax]
            ylim = [self._ymin/wRatio, self._ymax/wRatio]
        else :
            xlim = [self._xmin*wRatio, self._xmax*wRatio]
            ylim = [self._ymin, self._ymax]
        
        self.axes_.set_xlim(xlim)
        self.axes_.set_ylim(ylim)
        
    #def fit_winodw(self):
        

        

