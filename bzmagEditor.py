# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'bzmagEditor.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from lib.NOHTree import *
from lib.PropertyWidget import *
from lib.ConsoleWidget import *
from lib.ViewCanvas import *
from lib.TriangleMesh import *
from lib.MagnetoStaticSolver import *
import json

from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

# ------------------------------------------------------------------------------
class bzMagWindow(QMainWindow):
    def __init__(self, parent=None):
        super(bzMagWindow, self).__init__(parent)
        
        print('Init bzMag Window')
        self.setupUi()
        self.setWindowTitle('bzmag Editor v0.1 alpha')
        
        self.geometryRootName_ = 'geom'
        self.coordinateRootName_ = 'coordinate'
        self.materialRootName_ = 'material'
        self.BCRootName_ = 'BC'
        self.excitationRootName_ = 'excitation'
        
        self.hcoord = bzmag.new('Node', '/'+self.coordinateRootName_)
        self.hmat   = bzmag.new('Node', '/'+self.materialRootName_)
        self.hbc    = bzmag.new('Node', '/'+self.BCRootName_)
        self.hexcit = bzmag.new('Node', '/'+self.excitationRootName_)
        
        self.solver_ = MagnetoStaticSolver()
        
        #test 
        print('Prepair Test')
        self.test6();
        print('Test Complete')
        

        self.treewidget_node.build('/')
        self.mesh_ = None
        self.pot_ = None
        
        # 초기 선택모드 : 오브젝트
        self.canvas.setSelectMode(1)

        self.show()
        
    # ------------------------------------------------------------------------    
    def setupUi(self):
        # Centeral Widget
        self.centralwidget = QWidget(self)
        
        # NOHTree
        self.treewidget_node = NOHTree(self)
        
        # Qt5 Canvas
        self.canvas = ViewCanvas(self)
        
        # Spilter (화면을 좌우로 나눌것임, 좌:트리, 우:뷰)
        self.splitter = QSplitter(self.centralwidget)
        self.splitter.setEnabled(True)
        self.splitter.setOrientation(Qt.Horizontal)
        self.splitter.addWidget(self.treewidget_node)
        self.splitter.addWidget(self.canvas)
        self.splitter.setStretchFactor(0, 1)    # 화면의 1/3 
        self.splitter.setStretchFactor(1, 2)    # 화면의 2/3
        
        # Vertical Layout (하기 Spilter를 담을 컨테이너)
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout.addWidget(self.splitter)
        self.setCentralWidget(self.centralwidget)
        
        #tool bar
        toolbar = NavigationToolbar(self.canvas, self)
        self.addToolBar(toolbar)
        
        # Status Bar
        self.statusbar = QStatusBar(self)
        self.setStatusBar(self.statusbar)
        
        # Output Window (토킹윈도우)
        self.dockwidget_output = ConsoleWidget(self)
        self.addDockWidget(Qt.DockWidgetArea(8), self.dockwidget_output)
        
        # Property Window (도킹윈도우)
        self.dockwidget_property = PropertyWidget(self)
        self.addDockWidget(Qt.DockWidgetArea(2), self.dockwidget_property)
        
        # Menu Bar
        self.menubar = QMenuBar(self)
        self.setMenuBar(self.menubar)
        
        # ------------------------------------------------------------------------
        # File 메뉴 만들기
        self.menu_file = QMenu('파일(&F)',self.menubar)
        
        # Create new action
        self.actionOpen_O = QAction('열기(&O)', self)
        self.actionOpen_O.triggered.connect(qApp.quit)
        
        # Create new action
        self.actionSave_S = QAction('저장(&S)', self)
        self.actionSave_S.triggered.connect(qApp.quit)
        
        # Create new action
        self.actionSave_as_A = QAction('다른이름으로 저장(&A)', self)
        self.actionSave_as_A.triggered.connect(qApp.quit)
        
        # Create new action
        self.actionExit_E = QAction('끝내기(&Q)', self)
        self.actionExit_E.triggered.connect(qApp.quit)
        
        # 파일메뉴 Action  연결
        self.menu_file.addAction(self.actionOpen_O)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.actionSave_S)
        self.menu_file.addAction(self.actionSave_as_A)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.actionExit_E)
        
        
        # ------------------------------------------------------------------------
        # View 메뉴 만들기
        self.menu_view = QMenu('보이기(&V)', self.menubar)
        
        # Create new action
        self.actionViewMesh = QAction('요소 보이기', self)
        self.actionViewMesh.setCheckable(True)
        self.actionViewMesh.setChecked(True)
        self.actionViewMesh.toggled.connect(self.showMesh)
        
        # Create new action
        self.actionViewFluxline = QAction('플럭스도 보이기', self)
        self.actionViewFluxline.setCheckable(True)
        self.actionViewFluxline.setChecked(True)
        self.actionViewFluxline.toggled.connect(self.showFluxline)
        
        # Create new action
        self.actionViewFluxdensity = QAction('자속밀도 보이기', self)
        self.actionViewFluxdensity.setCheckable(True)
        self.actionViewFluxdensity.setChecked(True)
        self.actionViewFluxdensity.toggled.connect(self.showFluxdenisy)
        
        # Action 연결
        self.menu_view.addAction(self.actionViewMesh)
        self.menu_view.addAction(self.actionViewFluxline)
        self.menu_view.addAction(self.actionViewFluxdensity)
        
        
        # ------------------------------------------------------------------------
        # bzmag 메뉴 만들기
        self.menu_bzmag = QMenu('bzMag (&M)', self.menubar)
        menu_select = QMenu('선택모드', self.menu_bzmag)

        # Create new action
        self.actionMesh = QAction('요소나누기', self)
        self.actionMesh.triggered.connect(self.meshOperation)
        
        # Create new action
        self.actionAnalysis = QAction('해석하기', self)
        self.actionAnalysis.triggered.connect(self.solve)
        
        # Create new action
        self.actionSelectModeO = QAction('오브젝트', self, checkable=True, checked=True)
        self.actionSelectModeO.toggled.connect(self.selectMode)
        
        # Create new action
        self.actionSelectModeE = QAction('엣지', self, checkable=True, checked=False)
        self.actionSelectModeE.toggled.connect(self.selectMode)
        
        self.agSelectMode = QActionGroup(self)
        self.agSelectMode.addAction(self.actionSelectModeO)
        self.agSelectMode.addAction(self.actionSelectModeE)
        self.agSelectMode.setExclusive(True)
        
        # Action 연결
        self.menu_bzmag.addAction(self.actionMesh)
        self.menu_bzmag.addAction(self.actionAnalysis)
        self.menu_bzmag.addMenu(menu_select)
        menu_select.addAction(self.actionSelectModeO)
        menu_select.addAction(self.actionSelectModeE)
        
        
        # ------------------------------------------------------------------------
        # help 메뉴 만들기
        self.menu_help = QMenu('도움말(&H)', self.menubar)

        
        # ------------------------------------------------------------------------
        # 전체 메뉴 구성하기
        self.menubar.addAction(self.menu_file.menuAction())
        self.menubar.addAction(self.menu_view.menuAction())
        self.menubar.addAction(self.menu_bzmag.menuAction())
        self.menubar.addAction(self.menu_help.menuAction())
        
        # Signal priority is important!
        self.treewidget_node.itemSelectionChanged.connect(self.treewidget_node.changeSelectedNode)
        
        self.treewidget_node.itemsSelected.connect(lambda nodeIDs : self.dockwidget_property.nodesSelected(nodeIDs))
        self.treewidget_node.itemsSelected.connect(lambda nodeIDs : self.canvas.nodesSelected(nodeIDs))
        self.treewidget_node.itemAdded.connect(lambda nodeID: self.canvas.addItem(nodeID))
        
        self.dockwidget_property.itemUpdated.connect(lambda nodeID: self.canvas.updateItem(nodeID))
        self.dockwidget_property.nameUpdated.connect(lambda nodeID: self.treewidget_node.updateName(nodeID))
        
        self.canvas.itemsSelected.connect(lambda nodeIDs: self.treewidget_node.nodesSelected(nodeIDs))
        #self.graphics_view.itemUnSelectAll.connect(self.treewidget_node.unselectItemAllItem)
        
        QMetaObject.connectSlotsByName(self)

    # ------------------------------------------------------------------------    
    def createCurve(self, start, end, center, name):
        h   = bzmag.new('GeomHeadNode',      '/'+self.geometryRootName_+'/'+name)
        l   = bzmag.new('GeomCurveNode',     '/'+self.geometryRootName_+'/'+name+'/Curve')
        l.setParameters(start, end, center)
        return l
    
    # ------------------------------------------------------------------------
    def createCircle(self, center, radius, name):
        h   = bzmag.new('GeomHeadNode',      '/'+self.geometryRootName_+'/'+name)
        c   = bzmag.new('GeomCircleNode',    '/'+self.geometryRootName_+'/'+name+'/Circle')
        cov = bzmag.new('GeomCoverLineNode', '/'+self.geometryRootName_+'/'+name+'/Circle/Cover')
        c.setParameters(center, radius, '0')
        return cov
    
    # ------------------------------------------------------------------------    
    def createRectangle(self, point, dx, dy, name):
        h   = bzmag.new('GeomHeadNode',      '/'+self.geometryRootName_+'/'+name)
        r   = bzmag.new('GeomRectNode',      '/'+self.geometryRootName_+'/'+name+'/Rectangle')
        cov = bzmag.new('GeomCoverLineNode', '/'+self.geometryRootName_+'/'+name+'/Rectangle/Cover')
        r.setParameters(point, dx, dy)
        return cov
        
    # ------------------------------------------------------------------------
    def createBand(self, center, radius, width, name):
        h   = bzmag.new('GeomHeadNode',      '/'+self.geometryRootName_+'/'+name)
        b   = bzmag.new('GeomBandNode',      '/'+self.geometryRootName_+'/'+name+'/Band')
        cov = bzmag.new('GeomCoverLineNode', '/'+self.geometryRootName_+'/'+name+'/Band/Cover')
        b.setParameters(center, radius, width, '0')
        return cov
    
    # ------------------------------------------------------------------------        
    def booleanSubtract(self, lhs, rhs):
        path = lhs.getAbsolutePath()
        sub = bzmag.new('GeomSubtractNode', path+'/Subtract')
        
        for tool in rhs:
            tool_head = tool.getHeadNode()
            sub.attach(tool_head)
            tool_head.detach()
            sub.attach(tool_head)
            
        return sub
    
    # ------------------------------------------------------------------------        
    def booleanUnite(self, lhs, rhs):
        path = lhs.getAbsolutePath()
        sub = bzmag.new('GeomUniteNode', path+'/Unite')
        
        for tool in rhs:
            tool_head = tool.getHeadNode()
            sub.attach(tool_head)
            tool_head.detach()
            sub.attach(tool_head)
        return sub
        
    # ------------------------------------------------------------------------
    def clone(self, obj, name):
        path = obj.getAbsolutePath()
        ct = bzmag.new('GeomCloneToNode', path+'/CloneTo')
        
        bzmag.new('GeomHeadNode', '/'+self.geometryRootName_+'/'+name)
        cf = bzmag.new('GeomCloneFromNode', '/'+self.geometryRootName_+'/'+name+'/CloneFrom')
        cf.setCloneToNode(ct)
        return [ct, cf]
        
    # ------------------------------------------------------------------------
    def move(self, obj, dx, dy):
        path = obj.getAbsolutePath()
        m = bzmag.new('GeomMoveNode', path+'/Move')
        m.setParameters(dx, dy)
        return m
        
    # ------------------------------------------------------------------------
    def rotate(self, obj, angle):
        path = obj.getAbsolutePath()
        r = bzmag.new('GeomRotateNode', path+'/Rotate') 
        r.setParameters(angle)
        return r
        
    # ------------------------------------------------------------------------
    def spilt(self, obj, cs, pos):
        path = obj.getAbsolutePath()
        spilt = bzmag.new('GeomSpiltNode', path+'/Spilt')
        spilt.Orientation = pos
        if(cs != None) :
            spilt.CoordinateSystem = cs.getID()
            
        return spilt
        
    # ------------------------------------------------------------------------
    def createCS(self, origin, angle, name, parent=None):
        if parent != None:
            if parent.getTypeName() != 'CSNode' : return None
            
            parent_path = parent.getAbsolutePath()
            h = bzmag.new('CSNode', parent_path + '/' + name)
        else:
            h = bzmag.new('CSNode', '/' + self.coordinateRootName_ + '/' + name)
            
        h.setParameters(origin, angle)
        return h
    
    # ------------------------------------------------------------------------
    def createWinding(self, I, a, name):
        h = bzmag.new('WindingNode', '/' + self.excitationRootName_ + '/' + name)
        h.Current = I
        h.ParallelBranches = a
        
        return h
    
    # ------------------------------------------------------------------------
    def createCoil(self, Nc, direction, name, parent=None):
        if parent != None:
            if parent.getTypeName() != 'WindingNode' : return None
            
            parent_path = parent.getAbsolutePath()
            h = bzmag.new('CoilNode', parent_path + '/' + name)
            h.Direction = direction
            h.Turns = Nc
        
            return h
        else:
            return None
    
    # ------------------------------------------------------------------------
    def createExpression(self, key, expression, descriptioin):
        expr = bzmag.newobj('Expression')
        if not expr.setKey(key):
            pass
    
    # ------------------------------------------------------------------------
    def createMaterial(self, name):
        h = bzmag.new('MaterialNode', '/'+self.materialRootName_+'/'+name)
        return h
        
    # ------------------------------------------------------------------------    
    def createBC(self, type, node, idxs, value, name):
        bc = bzmag.new('BCNode', '/'+self.BCRootName_+'/'+name)
        for i in idxs:
            bc.addCurve(node, i)
        return bc
            
    # ------------------------------------------------------------------------
    def meshOperation(self):
        print('Prepair to generate the mesh')

        mesh = TriangleMesh()
        if not mesh.generateMesh(self.geometryRootName_, self.BCRootName_):
            return
            
        print('Complete to generate the mesh')
        
        self.mesh_ = mesh.output_
        nmesh = len(self.mesh_['triangles'])
        nvert = len(self.mesh_['vertices'])
        print('{0} elements ({1} vertices) are generated!!'.format(nmesh, nvert))
        self.canvas.setMeshData(mesh.output_)
        #self.canvas.visible_mesh(False)
        
    
    # ------------------------------------------------------------------------
    def calculateFluxdensity(self):
        return self.solver_.calculateFluxdensity()
        
        
    # ------------------------------------------------------------------------
    def solve(self):
        print('Solving...')

        # 메쉬 정보 셋팅 
        self.solver_.setMeshData(self.mesh_)
        
        # 재질정보 셋팅
        materials = bzmag.get(self.materialRootName_)
        self.solver_.setMaterials(materials)
        
        # 소스정보 셋팅 : 메쉬정보를 반드시 먼저 셋팅해야 한다
        source = bzmag.get(self.excitationRootName_)
        self.solver_.setSources(source)
        
        
        # 솔빙
        A = self.solver_.solve3()
        
        # 쇄교자속 계산
        self.solver_.calculate_fluxlinkage()
        
        # 플럭스도 뿌리기
        self.canvas.setPontentials(self.mesh_, A)
        #self.canvas.visible_fluxline(False)
        
        # 자속밀도 계산하기
        B = self.calculateFluxdensity()
        self.canvas.setFluxdensity(self.mesh_, B)
        
        # 토크 계산하기
        torque = self.solver_.calculate_torque(self.Band.getHeadNode(), 3)
        print(torque)
        
        print('Complete to solve!!')
    
    # ------------------------------------------------------------------------
    # 콜백함수
    def showMesh(self, state):
        if state:
            self.canvas.visible_mesh(True)
        else:
            self.canvas.visible_mesh(False)
        
    # ------------------------------------------------------------------------
    # 콜백함수
    def showFluxline(self, state):
        if state:
            self.canvas.visible_fluxline(True)
        else:
            self.canvas.visible_fluxline(False)
          
    # ------------------------------------------------------------------------
    # 콜백함수
    def showFluxdenisy(self, state):
        if state:
            self.canvas.visible_fluxdensity(True)
        else:
            self.canvas.visible_fluxdensity(False)
          
    # ------------------------------------------------------------------------
    def selectMode(self):
        if self.actionSelectModeO.isChecked() == True:
            self.canvas.setSelectMode(1)
        elif self.actionSelectModeE.isChecked() == True:
            self.canvas.setSelectMode(2)
        
    # ------------------------------------------------------------------------
    def test1(self):
        # bzMag Test
        h1   = bzmag.new('GeomHeadNode',      '/geom/h1')
        r1   = bzmag.new('GeomRectNode',      '/geom/h1/r1')
        c1   = bzmag.new('GeomCoverLineNode', '/geom/h1/r1/cover')
        ct   = bzmag.new('GeomCloneToNode',   '/geom/h1/r1/cover/cloneTo')
        rot1 = bzmag.new('GeomRotateNode',    '/geom/h1/r1/cover/cloneTo/rotate')
        r1.setParameters('-20, -20','40','40')
        rot1.setParameters('_pi/4')

        h2   = bzmag.new('GeomHeadNode',      '/geom/h2')
        r2   = bzmag.new('GeomCircleNode',    '/geom/h2/c1')
        c2   = bzmag.new('GeomCoverLineNode', '/geom/h2/c1/cover')
        sub2 = bzmag.new('GeomSubtractNode',  '/geom/h2/c1/cover/subtract')
        mov2 = bzmag.new('GeomMoveNode',      '/geom/h2/c1/cover/subtract/move')
        r2.setParameters('0,0', '100', '0')
        sub2.attach(h1)
        mov2.setParameters('50', '90')

        h3   = bzmag.new('GeomHeadNode',      '/geom/h3')
        cf   = bzmag.new('GeomCloneFromNode', '/geom/h3/cloneFrom')
        rot3 = bzmag.new('GeomRotateNode',    '/geom/h3/cloneFrom/rotate')
        cf.setCloneToNode(ct)
        rot3.setParameters('-_pi/3')
        
        h4   = bzmag.new('GeomHeadNode',      '/geom/h4')
        r4   = bzmag.new('GeomRectNode',      '/geom/h4/r1')
        rot4 = bzmag.new('GeomRotateNode',    '/geom/h4/r1/rotate')
        mov4 = bzmag.new('GeomMoveNode',      '/geom/h4/r1/rotate/move')
        c4   = bzmag.new('GeomCoverLineNode', '/geom/h4/r1/rotate/move/cover')
        r4.setParameters('30, 0','10','30')
        rot4.setParameters('_pi/12')
        mov4.setParameters('4','10')

        cs1  = bzmag.new('CSNode',            '/coordinate/cs1')
        cs2  = bzmag.new('CSNode',            '/coordinate/cs1/cs2')

        cs1.setParameters('0, 0', '_pi/3')
        cs2.setParameters('50, 0', '0')

        h1.CoordinateSystem = cs1.getID()
        
    # ------------------------------------------------------------------------
    def test2(self):
        c1 = self.createCircle('0, 0', '100', 'Stator')
        c2 = self.createCircle('0, 0', '90', 'Stator_In')
        c3 = self.createCircle('0, 0', '60', 'Shaft')
        
        ca1 = self.createCircle('0, 0', '70', 'Airgap1')
        ca2 = self.createCircle('0, 0', '70.5', 'Airgap2')
        
        [ct, cf] = self.clone(c2, 'StatorBore')
        sub = self.booleanSubtract(c1, [ct])
        print('Clone From Node :', cf)
        m1 = self.move(cf, '10', '10')
        cs1 = self.createCS('0, 0', '_pi/3', 'CS1')
        
        
    # ------------------------------------------------------------------------
    def test3(self):
        c1 = self.createCircle('0, 0', '100', 'Stator')
        c2 = self.createCircle('0, 0', '90', 'Stator_2')
        r1 = self.createRectangle('10, 10' ,'10', '10', 'hole')
        
        print('subtract start')
        s1 = self.booleanSubtract(c1, c2)
        
        print('move start')
        m1 = self.move(s1, '10', '30')
        
        print('rotate start')
        rot1 = self.rotate(m1, '_pi/2')
        
        print('move start')
        mov1 = self.move(r1, '3', '10')
        
        cs1 = self.createCS('5, 0', '_pi/4', 'CS1')
        mov1.getParent().CoordinateSystem = cs1.getID()
        
    # ------------------------------------------------------------------------
    # 가장 간단한 자기회로 해석
    def test4(self):
        # left, top, right, bottom
        r1 = self.createRectangle('-10, -10', '20', '20', 'Core')
        c1 = self.createCircle('0, 0', '10', 'CoreInner')
        core1 = self.booleanSubtract(r1, [c1])
        #r2 = self.createRectangle('0, -1', '15', '2', 'Airgap')
        #core2 = self.booleanSubtract(core1, [r2])
        
        
        c3 = self.createCircle('0, 0', '10.000000', 'Dummy')
        r3 = self.createRectangle('-10, -10', '20', '15', 'Dummy_Cut')
        core2 = self.booleanSubtract(c3, [r3])
        
        
        background = self.createRectangle('-50, -50', '100', '100', 'Background')
        
        #l = self.createCurve('-5,0','5,0','0,0', 'TestCurve')
        #print('rotate start')
        #rot1 = self.rotate(l, '_pi/2')
        
        #print('move start')
        #mov1 = self.move(rot1, '3', '10')
        
        cs1 = self.createCS('5, 0', '0', 'CS1')
        cs2 = self.createCS('0, 0', '_pi/30', 'CS2', cs1)
        
        bc1 = self.createBC(1, background.getHeadNode(), [0,1,2,3], 0, 'Fixed')
        
        
        mat1 = self.createMaterial('vaccum')
        mat2 = self.createMaterial('copper')
        mat3 = self.createMaterial('core')
        
        #l.getParent().CoordinateSystem = cs2.getID()
        
    # ------------------------------------------------------------------------
    # 가장 간단한 자기회로 해석
    def test5(self):
        # left, top, right, bottom
        r1 = self.createRectangle('-10, -10', '20', '20', 'Core')
        r2 = self.createRectangle('-5, -5', '10', '10', 'CoreInner')
        core1 = self.booleanSubtract(r1, [r2])
        
        r3 = self.createRectangle('5, -1', '5', '2', 'Magnet')
        background = self.createRectangle('-50, -50', '100', '100', 'Background')
        
        cs1 = self.createCS('0, 0', '_pi/6', 'CS1')
        
        mat1 = self.createMaterial('Magnet')
        mat1.Magnetization = 1.2
        mat1.Mvector = (0,1)
        mat2 = self.createMaterial('Core')
        mat2.Permeability = 3000
        
        r1_head = r1.getHeadNode()
        r1_head.Material = mat2.getID()
        r1_head.RequiredNumberOfElements = 1000
        
        r3_head = r3.getHeadNode()
        r3_head.CoordinateSystem = cs1.getID()
        r3_head.Material = mat1.getID()
        r3_head.RequiredNumberOfElements = 200
        
    # ------------------------------------------------------------------------
    # 모터해석
    def test6(self):
        mat1 = self.createMaterial('Magnet')
        mat1.Magnetization = 1.2
        mat1.Mvector = (1,0)
        mat2 = self.createMaterial('Core')
        mat2.Permeability = 3000
        
        # 회전자
        mag_arc_ratio = 0.75
        
        rotor = self.createCircle('0, 0', '60', 'Rotor')
        [rotor, rotor_clone] = self.clone(rotor, 'RotorBore')
        magnet = self.createCircle('0, 0', '65', 'Magnet1')
        magnet = self.booleanSubtract(magnet, [rotor_clone])
        rotor.getHeadNode().Material = mat2.getID()
        
        # 자화방향 CS
        csMag = []
        for i in range(4):
            csMag.append(self.createCS('0, 0', '_pi/4*' + str(1+i*2) + str('+ _pi*') + str(i%2), 'CS_Mag' + str(i+1)))
            
            
        cs_mag_cut = self.createCS('0, 0', '_pi/2 * 0.75', 'CS_MagCut1')
        magnet = self.spilt(magnet, None, True)
        magnet = self.spilt(magnet, cs_mag_cut, False)
        magnet = self.rotate(magnet, '_pi/2 * (1-0.75)/2')
        magnet.getHeadNode().CoordinateSystem = csMag[0].getID()
        magnet.getHeadNode().Material = mat1.getID()

            
        for i in range(1,4):
            [ct, new_magnet] = self.clone(magnet, 'Magnet' + str(i+1))
            new_magnet = self.rotate(new_magnet, '_pi / 2 * ' + str(i))
            new_magnet.getHeadNode().CoordinateSystem = csMag[i].getID()
            new_magnet.getHeadNode().Material = mat1.getID()
            
        
        # 고정자
        stator_by = self.createCircle('0, 0', '110', 'Stator')
        stator_by_in = self.createCircle('0, 0', '100', 'Stator_in')
        stator_by = self.booleanSubtract(stator_by, [stator_by_in])
        stator_by.getHeadNode().Material = mat2.getID()
        
        
        shoe1 = self.createCircle('0, 0', '66.5', 'Shoe1')
        shoe1_in = self.createCircle('0, 0', '65.5', 'Shoe1_in')
        [shoe1, shoe2_in] = self.clone(shoe1, 'Shoe2_in')
        shoe1 = self.booleanSubtract(shoe1, [shoe1_in])
        
        shoe2 = self.createCircle('0, 0', '67.5', 'Shoe2')
        shoe2 = self.booleanSubtract(shoe2, [shoe2_in])
        
        
        slot_open11 = self.createRectangle('0, -1', '100', '2', 'SlotOpen11')
        [dd, slot_open12] = self.clone(slot_open11, 'SlotOpen12')
        [dd, slot_open21] = self.clone(slot_open11, 'SlotOpen21')
        [dd, slot_open22] = self.clone(slot_open11, 'SlotOpen22')
        
        slot_open12 = self.rotate(slot_open12, '_pi/12')
        slot_open22 = self.rotate(slot_open22, '_pi/12')
        
        shoe1 = self.booleanSubtract(shoe1, [slot_open11, slot_open12])
        shoe2 = self.booleanSubtract(shoe2, [slot_open21, slot_open22])
        
        cs_tooth_cut = self.createCS('0, 0', '_pi/12', 'CS_ToothCut1')
        shoe1 = self.spilt(shoe1, None, True)
        shoe1 = self.spilt(shoe1, cs_tooth_cut, False)
        shoe1 = self.rotate(shoe1, '-_pi/12/2')
        
        shoe2 = self.spilt(shoe2, None, True)
        shoe2 = self.spilt(shoe2, cs_tooth_cut, False)
        shoe2 = self.rotate(shoe2, '-_pi/12/2')
        
        tooth = self.createRectangle('66.5, -5', '40', '10', 'Tooth')
        tooth = self.booleanUnite(tooth, [shoe1, shoe2])
        stator_by = self.booleanUnite(stator_by, [tooth])
        for i in range(0,23):
            [ct, new_tooth] = self.clone(tooth, 'Tooth' + str(i+1))
            new_tooth = self.rotate(new_tooth, '_pi / 12 * ' + str(i+1))
            stator = self.booleanUnite(stator_by, [new_tooth])
            
        stator = self.rotate(stator, '_pi/180*1')


        # 더미 오브젝트
        AirStator = self.createCircle('0, 0', '110', 'AirStator')
        AirRotor = self.createCircle('0, 0', '65', 'AirRotor')
        AirModel = self.createCircle('0, 0', '150', 'AirModel')
        #Airgap1 = self.createCircle('0, 0', '65.2', 'Airgap1')
        #Airgap2 = self.createCircle('0, 0', '65.3', 'Airgap2')
        self.Band      = self.createBand('0, 0', '65.25', '0.2', 'Airgap')
        
        # 샤프트
        shaft = self.createCircle('0, 0', '30', 'Shaft')
        
        
        
        # 코일
        coil = self.createRectangle('70, 5', '25', '3', 'Coil')
        coil = self.rotate(coil, '_pi/12*3')
        coil = self.rotate(coil, '_pi/180*1')
        
        recoil = self.createRectangle('70, -5', '25', '-3', 'ReCoil')
        recoil = self.rotate(recoil, '_pi/180*1')
        

        
        # 경계조건 설정
        bc1 = self.createBC(1, AirModel.getHeadNode(), [0,1], 0, 'Fixed')
        
        
        # Excitation 설정
        winding1 = self.createWinding(0, 1, 'PhaseA')
        coil1 = self.createCoil(10, True, 'PhA', winding1)
        coil1.setReferenceNode(coil.getHeadNode())
        
        coil2 = self.createCoil(10, False, 'RePhA', winding1)
        coil2.setReferenceNode(recoil.getHeadNode())
        
        
    # ------------------------------------------------------------------------
    def test7(self):
        # 회전자
        mag_arc_ratio = 0.75
        
        rotor = self.createCircle('0, 0', '50', 'Rotor')
        [rotor, rotor_clone] = self.clone(rotor, 'RotorBore')
        magnet = self.createCircle('0, 0', '65', 'Magnet')
        magnet = self.booleanSubtract(magnet, [rotor])
        
        cs_mag_cut = self.createCS('0, 0', '_pi/2 * 0.72', 'CS_MagCut1')
        magnet = self.spilt(magnet, None, True)
        magnet = self.spilt(magnet, cs_mag_cut, False)
        magnet = self.rotate(magnet, '_pi/2 * (1-0.72)/2')
        
        for i in range(0,3):
            [ct, new_magnet] = self.clone(magnet, 'Magnet' + str(i+1))
            new_magnet = self.rotate(new_magnet, '_pi / 2 * ' + str(i+1))
        
        AirRotor = self.createCircle('0, 0', '65.00000000000000', 'AirRotor')
        bc1 = self.createBC(1, AirRotor.getHeadNode(), [0,1], 0, 'Fixed')
    
    # ------------------------------------------------------------------------    
    def test8(self):
        Band      = self.createBand('0, 0', '65.25', '0.5', 'Airgap')